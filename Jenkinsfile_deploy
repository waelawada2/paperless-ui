#!/usr/bin/env groovy

properties([
  parameters([
    string(name: 'DOCKER_TAG', defaultValue: '', description: 'Optional. The Docker image tag, if deploying image.'),
    string(name: 'VERSION', defaultValue: '1.0.0-SNAPSHOT', description: 'The artifact version for deployment.',),
    booleanParam(name: 'WITH_PROMOTION', description: 'Allow promotions to higher environments', defaultValue: false),
    string(name: 'BRANCH_NAME', defaultValue: 'develop', description: 'The branch used for checking out "Jenkinsfile_deploy".')
  ]),
  buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '5')),
  disableConcurrentBuilds()
])

@Library('sothebys-jenkins-shared-library-javascript@1.x') _

def that = this

try {

  node {

    stage('Prepare') {
      deleteDir()
      checkout scm
    }

    // Create Docker image if none was provided as parameter
    if (!params.DOCKER_TAG) {
      stage('Fetch artifacts') {
        fetchArtifact {
          version = that.params.VERSION
        }
      }

      stage('Create Docker image') {
        createDockerImage {}
      }

      stage('Push Docker image') {
        pushDockerImageToRegistry {}
      }
    }

    stage("Deploy to ${env.DEPLOYMENT_ENVIRONMENT.toUpperCase()}") {
      deployToECS {}
    }

  } //node

  if (params.WITH_PROMOTION) {
    promoteDeployment {}
  }

} catch (e) {
  currentBuild.result = 'FAILURE'
  throw e
} finally {
  sendBuildNotification {}
}
