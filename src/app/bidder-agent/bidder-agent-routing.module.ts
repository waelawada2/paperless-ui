import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  BidderAgentComponent,
  LotsListComponent,
  LotExecutionComponent,
  AuctionsListComponent,
  AuctionLotsComponent,
} from './routed-components';
import { LogonGuard, BidxRoleGuard } from '@paperless/shared/security'
import {
  LotsPerClientResolver,
  BidderDataResolver,
  AuctionsCardsPerAgentResolver,
  LotsPerAgentResolver
} from './services';

const routes: Routes = [
  {
    path: '',
    component: BidderAgentComponent,
    canActivate: [LogonGuard, BidxRoleGuard],
    children: [
      {
        path: 'auctions',
        component: AuctionsListComponent,
        resolve: {
          auctionsCards: AuctionsCardsPerAgentResolver,
        },
        children: [
          {
            path: 'lots/:auctionId/session/:sessionId',
            component: AuctionLotsComponent,
            resolve: {
              lotsAndAuction: LotsPerAgentResolver,
            }
          }
        ]
      },
      {
        path: 'auction/:auctionId/session/:sessionId',
        component: LotsListComponent,
        children: [
          {
            path: 'lot/:lotId/client/:clientId',
            component: LotExecutionComponent,
            resolve: {
              lots: LotsPerClientResolver,
              client: BidderDataResolver,
            }
          }
        ]
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BidderAgentRoutingModule { }
