import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { WinningConfirmationModalComponent } from './winning-confirmation-modal.component';
import { MoneyPipe } from '@paperless/shared/pipes';
import { PriceConfirmationComponent, KeypadComponent } from '@paperless/shared/components';
import { CommonModule } from '@angular/common';

describe('WinningConfirmationModalComponent', () => {
  let component: WinningConfirmationModalComponent;
  let fixture: ComponentFixture<WinningConfirmationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WinningConfirmationModalComponent, MoneyPipe, PriceConfirmationComponent, KeypadComponent ],
      providers: [
        {
          provide: NgbActiveModal,
          useClass: class extends NgbActiveModal {
            close() {}
            dismiss() {}
          }
        }
      ],
      imports: [
        CommonModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WinningConfirmationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
