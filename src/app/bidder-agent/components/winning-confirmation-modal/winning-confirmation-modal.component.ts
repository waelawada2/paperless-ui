import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-winning-confirmation-modal',
  templateUrl: './winning-confirmation-modal.component.html',
  styleUrls: ['./winning-confirmation-modal.component.scss']
})
export class WinningConfirmationModalComponent implements OnInit {

  currencyCode = '';
  keypadFor = 'bidder';

  constructor(private  ngbActiveModal: NgbActiveModal) { }

  ngOnInit() {
  }

  onCancel() {
    this.ngbActiveModal.dismiss();
  }

  onPriceConfirmed(price: number) {
    this.ngbActiveModal.close(price);
  }

}
