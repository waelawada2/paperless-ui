import { Component, OnInit, Input } from '@angular/core';
import { BidLot } from '@paperless/shared';

@Component({
  selector: 'app-client-lots-panel',
  templateUrl: './client-lots-panel.component.html',
  styleUrls: ['./client-lots-panel.component.scss']
})
export class ClientLotsPanelComponent implements OnInit {

  @Input() lots = new Array<BidLot>();

  constructor() { }

  ngOnInit() {
  }

}
