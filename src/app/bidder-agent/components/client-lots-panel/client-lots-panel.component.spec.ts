import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientLotsPanelComponent } from './client-lots-panel.component';
import { ErrorPanelComponent } from '@paperless/shared/components';

describe('ClientLotsPanelComponent', () => {
  let component: ClientLotsPanelComponent;
  let fixture: ComponentFixture<ClientLotsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientLotsPanelComponent, ErrorPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientLotsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
