import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { UnderbidConfirmationModalComponent } from './underbid-confirmation-modal.component';
import { PriceConfirmationComponent, KeypadComponent } from '@paperless/shared/components';
import { MoneyPipe } from '@paperless/shared/pipes';

describe('UnderbidConfirmationModalComponent', () => {
  let component: UnderbidConfirmationModalComponent;
  let fixture: ComponentFixture<UnderbidConfirmationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnderbidConfirmationModalComponent, PriceConfirmationComponent, KeypadComponent, MoneyPipe ],
      providers: [
        {
          provide: NgbActiveModal,
          useClass: class extends NgbActiveModal {
            close() {}
            dismiss() {}
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnderbidConfirmationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
