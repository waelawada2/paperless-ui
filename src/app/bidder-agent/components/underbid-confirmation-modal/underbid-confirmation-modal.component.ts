import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-underbid-confirmation-modal',
  templateUrl: './underbid-confirmation-modal.component.html',
  styleUrls: ['./underbid-confirmation-modal.component.scss']
})
export class UnderbidConfirmationModalComponent implements OnInit {

  currencyCode = '';
  keypadFor = 'bidder';

  constructor(private  ngbActiveModal: NgbActiveModal) { }

  ngOnInit() {
  }

  onCancel() {
    this.ngbActiveModal.dismiss();
  }

  onPriceConfirmed(price: number) {
    this.ngbActiveModal.close(price);
  }

}
