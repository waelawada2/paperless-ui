import { Component, Input,  OnChanges, SimpleChanges } from '@angular/core';
import { BidClient, ClientPartyType } from '@paperless/shared';

@Component({
  selector: 'app-client-panel',
  templateUrl: './client-panel.component.html',
  styleUrls: ['./client-panel.component.scss']
})
export class ClientPanelComponent implements OnChanges {

  @Input()
  client: BidClient;

  isIndividual: Boolean;

  constructor(
    private clientPartyType: ClientPartyType) { }


  ngOnChanges(changes: SimpleChanges): void {
    this.isIndividual = this.clientPartyType.assessPartyType(changes.client.currentValue);
  }

}
