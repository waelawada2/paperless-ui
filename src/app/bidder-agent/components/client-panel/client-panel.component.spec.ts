import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientPanelComponent } from './client-panel.component';
import { ClientPartyType, SharedModule } from '@paperless/shared';

describe('ClientComponent', () => {
  let component: ClientPanelComponent;
  let fixture: ComponentFixture<ClientPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientPanelComponent],
      providers: [
        ClientPartyType,
      ],
      imports: [
        SharedModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientPanelComponent);
    component = fixture.componentInstance;
    component.client = {} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
