import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotInformationComponent } from './lot-information.component';
import { SharedModule } from '@paperless/shared';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

describe('LotInformationComponent', () => {
  let component: LotInformationComponent;
  let fixture: ComponentFixture<LotInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotInformationComponent ],
      imports: [
        SharedModule.forRoot(),
      ],
      providers: [
        {
          provide: NgbModal,
          useClass: class {
            open() {
              return {} as NgbModalRef;
            }
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotInformationComponent);
    component = fixture.componentInstance;
    component.lot = {} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
