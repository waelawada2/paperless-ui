import {
    Component,
    OnInit,
    Input,
    SimpleChanges,
    OnChanges,
    Output,
    EventEmitter,
    Type
} from '@angular/core';
import { BidLot } from '@paperless/shared';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  NobidConfirmationModalComponent,
} from '../nobid-confirmation-modal/nobid-confirmation-modal.component';
import {
  WinningConfirmationModalComponent,
} from '../winning-confirmation-modal/winning-confirmation-modal.component'
import {
  UnderbidConfirmationModalComponent,
} from '../underbid-confirmation-modal/underbid-confirmation-modal.component';

@Component({
  selector: 'app-lot-information',
  templateUrl: './lot-information.component.html',
  styleUrls: ['./lot-information.component.scss']
})
export class LotInformationComponent implements OnInit, OnChanges {

  @Input() lot: BidLot;
  @Output() lotAction = new EventEmitter<{ lot: BidLot, action: string, payload?: any }>();

  imageLoaded = false;
  constructor(private modal: NgbModal) { }

  ngOnInit() {
  }

  openNoBidConfirmation(): void {
    this.createModal(NobidConfirmationModalComponent)
      .result.then(_ => this.lotAction.emit({ lot: this.lot, action: 'nobid' })).catch(() => false);
  }

  openUnderbidConfirmation(): void {
    const modalRef = this.createModal(UnderbidConfirmationModalComponent);
    modalRef.result.
              then((amount) => this.lotAction.emit({ lot: this.lot, action: 'underbid', payload: { amount } }))
              .catch(() => false);
  }

  openWinningConfirmation(): void {

    const modalRef = this.createModal(WinningConfirmationModalComponent);
    modalRef.result.
              then((amount) => this.lotAction.emit({ lot: this.lot, action: 'won', payload: { amount } }))
              .catch(() => false);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.imageLoaded = false;
  }

  onImageLoaded() {
    this.imageLoaded = true;
  }

  private createModal<T extends { currencyCode: string }>(modalComponent: Type<T>) {
    const modalRef = this.modal.open(modalComponent, {
      keyboard: false,
      windowClass: 'vertical-centered',
    });
    const component = modalRef.componentInstance as T;
    component.currencyCode = this.lot.currency;
    return modalRef;
  }

}
