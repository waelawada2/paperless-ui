import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-nobid-confirmation-modal',
  templateUrl: './nobid-confirmation-modal.component.html',
  styleUrls: ['./nobid-confirmation-modal.component.scss']
})
export class NobidConfirmationModalComponent implements OnInit {

  currencyCode = '';

  constructor(private  ngbActiveModal: NgbActiveModal) { }

  ngOnInit() {
  }

  onCancel() {
    this.ngbActiveModal.dismiss();
  }

  confirm() {
    this.ngbActiveModal.close();
  }

}
