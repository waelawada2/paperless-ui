import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { NobidConfirmationModalComponent } from './nobid-confirmation-modal.component';
import { PriceConfirmationComponent, KeypadComponent } from '@paperless/shared/components';
import { MoneyPipe } from '@paperless/shared/pipes';

describe('NobidConfirmationModalComponent', () => {
  let component: NobidConfirmationModalComponent;
  let fixture: ComponentFixture<NobidConfirmationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NobidConfirmationModalComponent, PriceConfirmationComponent, KeypadComponent, MoneyPipe ],
      providers: [
        {
          provide: NgbActiveModal,
          useClass: class extends NgbActiveModal {
            close() {}
            dismiss() {}
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NobidConfirmationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
