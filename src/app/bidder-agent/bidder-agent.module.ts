import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BidderAgentRoutingModule } from './bidder-agent-routing.module';

import { SharedModule } from '@paperless/shared';
import {
  LotsPerClientResolver,
  BidderDataResolver,
  AuctionsCardsPerAgentResolver,
  LotsPerAgentResolver
} from './services';

import {
  LotInformationComponent,
  ClientLotsPanelComponent,
  ClientPanelComponent,
  WinningConfirmationModalComponent,
  UnderbidConfirmationModalComponent,
  NobidConfirmationModalComponent,
} from './components';
import {
  BidderAgentComponent,
  LotExecutionComponent,
  LotsListComponent,
  AuctionsListComponent,
  AuctionLotsComponent,
} from './routed-components';

@NgModule({
  imports: [
    CommonModule,
    BidderAgentRoutingModule,
    SharedModule,
  ],
  declarations: [
    BidderAgentComponent,
    ClientLotsPanelComponent,
    ClientPanelComponent,
    LotExecutionComponent,
    LotInformationComponent,
    LotsListComponent,
    AuctionsListComponent,
    AuctionLotsComponent,
    WinningConfirmationModalComponent,
    UnderbidConfirmationModalComponent,
    NobidConfirmationModalComponent,
  ],
  providers: [
    LotsPerClientResolver,
    BidderDataResolver,
    LotsPerAgentResolver,
    AuctionsCardsPerAgentResolver,
  ],
  entryComponents: [
    WinningConfirmationModalComponent,
    UnderbidConfirmationModalComponent,
    NobidConfirmationModalComponent,
  ]
})
export class BidderAgentModule { }
