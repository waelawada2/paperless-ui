import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuctionService } from '@paperless/shared/services';
import { SessionService } from '@paperless/shared/security/';
import { switchMap } from 'rxjs/operators';
import { graceFail } from '@paperless/shared/rxjs/operators';

@Injectable()
export class LotsPerAgentResolver implements Resolve<any> {
    constructor(
        private auctionService: AuctionService,
        private sessionService: SessionService,
    ) {

    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const { auctionId, sessionId } = route.params;
        return this.auctionService.getLotsAndAuctionByAgent(auctionId, sessionId)
            .pipe(
                graceFail({
                    lots: [],
                    auction: {},
                })
            );
    }
}
