import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { BidLot } from '@paperless/shared';
import { AuctionService } from '@paperless/shared/services';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

@Injectable()
export class LotsPerClientResolver implements Resolve<BidLot[]> {
    constructor(private auctionService: AuctionService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const { auctionId, sessionId } = route.parent.params;
        const { clientId } = route.params;
        return this.auctionService
                .getLotsPerClient(clientId, auctionId, +sessionId)
                .pipe(map((obj) => obj.lots))
                .pipe(catchError((err) => of(null)));
    }
}
