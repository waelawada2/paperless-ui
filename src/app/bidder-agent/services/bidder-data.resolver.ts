import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { BidClient } from '@paperless/shared';
import { BidClientService } from '@paperless/shared/services';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

@Injectable()
export class BidderDataResolver implements Resolve<BidClient> {
    constructor(private bidClient: BidClientService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        // our parent route holds :auctionId, :sessionId
        const { auctionId, sessionId } = route.parent.params;
        const { lotId, clientId } = route.params;
        return this.bidClient.getClientDataForLot(clientId, auctionId, sessionId, lotId)
            .pipe(catchError((err) => of(null)));
    }
}
