import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionService } from '@paperless/shared/security';
import { AuctionService } from '@paperless/shared/services';
import { switchMap } from 'rxjs/operators';
// tslint:disable-next-line
import { Auction } from '@paperless/shared';
import { graceFail } from '@paperless/shared/rxjs/operators';

@Injectable()
export class AuctionsCardsPerAgentResolver implements Resolve<{ cards: Auction[] }> {
    constructor(
        private auctionService: AuctionService,
        private sessionService: SessionService,
    ) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.auctionService.getAuctionCardsByAgent()
        .pipe(
            graceFail({ cards : []})
        );
    }
}
