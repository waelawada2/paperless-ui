import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionsListComponent } from './auctions-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { SharedModule } from '@paperless/shared';
import { of } from 'rxjs/observable/of';

describe('[Bidder]AuctionsListComponent', () => {
  let component: AuctionsListComponent;
  let fixture: ComponentFixture<AuctionsListComponent>;

  beforeEach(async(() => {
    const activatedRouteData = {
      auctionsCards: {
        cards: [],
      }
    };
    TestBed.configureTestingModule({
      declarations: [ AuctionsListComponent ],
      imports: [
        RouterTestingModule,
        SharedModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of(activatedRouteData),
            snapshot: {
              data: activatedRouteData,
            },
            children: []
          },
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
