import { Component, OnInit, AfterViewInit, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Auction } from '@paperless/shared';
import { AuctionCardComponent } from '@paperless/shared/components';

@Component({
  selector: 'app-auctions-list',
  templateUrl: './auctions-list.component.html',
  styleUrls: ['./auctions-list.component.scss']
})
export class AuctionsListComponent implements OnInit, AfterViewInit {

  auctions: Auction[] = [];
  @ViewChildren(AuctionCardComponent) auctionCards: QueryList<AuctionCardComponent>;
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const { cards } = this.activatedRoute.snapshot.data.auctionsCards as { cards: Auction[] };
    cards.forEach((card) => {
      if (!!!card.session) {
        card.session = 0;
      }
    });
    this.auctions = cards;
  }

  ngAfterViewInit() {
    const [auctionLotsRoute] = this.activatedRoute.children;
    if (auctionLotsRoute) {
      const { auctionId } = auctionLotsRoute.snapshot.params;
      const [matchedAuction] = this.auctionCards.filter(card => card.auction.auctionId === auctionId);
      if (matchedAuction) {
        const nativeElement = matchedAuction.elementRef.nativeElement;
        if (nativeElement.scrollIntoView) {
          nativeElement.scrollIntoView();
        }
      }
    }
  }

}
