
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotsListComponent } from './lots-list.component';
import { SharedModule } from '@paperless/shared';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigModule } from '@paperless/shared/config';
import { SessionService } from '@paperless/shared/security';
import { of } from 'rxjs/observable/of';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LotInformationComponent } from '@paperless/bidder-agent/components';

describe('LotsListComponent', () => {
  let component: LotsListComponent;
  let fixture: ComponentFixture<LotsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ConfigModule,
        SharedModule.forRoot(),
        RouterTestingModule,
        NgbModule.forRoot(),
      ],
      declarations: [ LotsListComponent, LotInformationComponent ],
      providers: [
        {
          provide: SessionService,
          useClass: class {
            get userProfile$() {
              return of({});
            }
            getUserAttrOrThrow(attr: string) {
              return of(attr);
            }
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
