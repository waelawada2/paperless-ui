import { Component, OnInit, ViewChildren, AfterViewInit, QueryList } from '@angular/core';
import { BidLot, AuctionCard } from '@paperless/shared';
import { AuctionService, LoadingScreenService } from '@paperless/shared/services';
import { Router, ActivatedRoute } from '@angular/router';
import { map, catchError, switchMap, take } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { SessionService } from '@paperless/shared/security';
import { LotCardComponent } from '@paperless/shared/components';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';


@Component({
  selector: 'app-lots-list',
  templateUrl: './lots-list.component.html',
  styleUrls: ['./lots-list.component.scss']
})
export class LotsListComponent implements OnInit, AfterViewInit {

  @ViewChildren('allLotsCards') allLotsCards: QueryList<LotCardComponent>;

  errorsLoadingLots = false;
  selectedLot: BidLot;
  session: number;
  loadedAuction: AuctionCard;
  lotExecutionQueue$: Observable<{ lot: BidLot, action: string, payload?: string, auctionId: string, session: number }>;
  get loadedLots() {
    return this._loadedLots;
  }
  private _loadedLots: BidLot[] = [];
  // Temporal conts
  private lotExecutionSubject = new Subject<{ lot: BidLot, action: string, payload?: string, auctionId: string, session: number }>();

  constructor(
    private auctionService: AuctionService,
    private router: Router,
    private sessionService: SessionService,
    private activatedRoute: ActivatedRoute,
    private loadingScreen: LoadingScreenService,
  ) {
    this.lotExecutionQueue$ = this.lotExecutionSubject.asObservable();
  }

  ngOnInit() {
    const { sessionId } = this.activatedRoute.snapshot.params;
    this.session = +sessionId;
    this.loadInitialLots();
  }

  ngAfterViewInit() {

    /**
     * This component holds a <router-outlet> wich displays the selected lot
     * When the user refresh the page, we want to mark again the selected lot
     * With the help of routerLinkActive we can get that for free,
     * however, we want also to scroll the view to that single component,
     */
    if (this.activatedRoute.children.length) {
      // Get the route where we loaded our lot info
      // That helps us to get the :lotId param from it
      const [lotInfoRoute] = this.activatedRoute.children;
      const lotInfoRouteParams = lotInfoRoute.params.pipe(take(1));
      const allLots$ = this.allLotsCards
        .changes.pipe(take(1));

      combineLatest(
        lotInfoRouteParams,
        allLots$ as Observable<QueryList<LotCardComponent>>,
      ).subscribe(([routeParams, lotsCmp]) => {
        const selectedLotId = routeParams.lotId;
        const [selectedLotCmp] = lotsCmp.filter( lotCmp => lotCmp.lot.lotId === selectedLotId );
        if (selectedLotCmp) {
          /**
           * async handling of ExpressionChangedAfterItHasBeenCheckedError
           * Since we are changing an input from a life cycle method
           */
          Promise.resolve(null).then(() => this.selectedLot = selectedLotCmp.lot);
          this.scrollToLotCard(selectedLotCmp);
        }
      });
    }

  }

  getLotsAndAuction() {
    const { auctionId, sessionId } = this.activatedRoute.snapshot.params
    return this.auctionService.getLotsAndAuctionByAgent(auctionId, +sessionId)
                .pipe(map((rs) => ({success: true, lots: rs.lots, auction: rs.auction })))
                .pipe(catchError(_ => this.handleError()));
  }

  locateLotCardByLotId(lotId: string) {
    const [lotCard] = this.allLotsCards.filter(lotCardCmp => lotCardCmp.lot.lotId === lotId);
    return lotCard;
  }

  scrollToLotCard(lotCard: LotCardComponent) {
      if (lotCard.elementRef.nativeElement.scrollIntoView) {
        lotCard.elementRef.nativeElement.scrollIntoView();
      }
  }

  goToLotDetails(lot: BidLot) {
    this.router.navigate(['./lot', lot.lotId, 'client', lot.clientContact.clientNumber], {
      relativeTo: this.activatedRoute,
    });
  }

  selectLot(lot: BidLot) {
    this.selectedLot = lot;
  }


  onLotAction($event: { lot: BidLot, action: string, payload?: any }) {
    this.lotExecutionSubject.next({
      ...$event,
      auctionId: this.loadedAuction.auctionId,
      session: this.session,
    });
  }

  goToNextLot(relativeLot: BidLot) {
    const nextLot = this.findNextLotByCondition(_lot => _lot.lotId !== relativeLot.lotId, relativeLot);
    this.goToLot(nextLot);
  }

  goToNextAvailableLot(relativeLot: BidLot) {
    const nextLot = this.getNextAvailableLot(relativeLot);
    if (nextLot) {
      this.goToLot(nextLot);
    }
  }

  private goToLot(lot: BidLot) {
      const lotCardToGo = this.locateLotCardByLotId(lot.lotId);
      this.selectLot(lot);
      this.scrollToLotCard(lotCardToGo);
      this.goToLotDetails(lot);
  }

  private getNextAvailableLot(relativeLot: BidLot) {
    const matcher = (_lot: BidLot) => _lot.lotId !== relativeLot.lotId;
    return this.findNextLotByCondition(matcher, relativeLot);
  }

  private findNextLotByCondition(conditionMatcher: (lot: BidLot) => boolean, relativeLot: BidLot) {
    const lots = this._loadedLots;
    const index = lots.indexOf(relativeLot);
    // Look for lots ahead of the current one that were not confirmed yet
    let lotsPortion = lots.slice(index);
    let lotsCandidate = lotsPortion.filter(conditionMatcher);
    let lotToGo: BidLot = null;
    // Didnt match any load ahead of the current one, so we look for the ones before
    if (!lotsCandidate.length) {
      // look for lots earlier of the current one that were not confirmed yet
      lotsPortion = lots.slice(0, index);
      lotsCandidate = lotsPortion.filter(conditionMatcher);
    }
    [lotToGo] = lotsCandidate;
    return lotToGo;
  }

  private loadInitialLots() {
    this.getLotsAndAuction().pipe(
      this.loadingScreen.asRxJsOperator(),
      take(1),
    )
      .subscribe(rs => {
        this.errorsLoadingLots = !rs.success;
        this._loadedLots = rs.lots;
        this.loadedAuction = rs.auction;
      });
  }

  private handleError() {
    return of({ success: false, lots: [] as BidLot[], auction: null, })
  }


}
