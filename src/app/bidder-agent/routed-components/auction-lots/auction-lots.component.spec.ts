import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionLotsComponent } from './auction-lots.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@paperless/shared';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs/observable/of';

describe('AuctionLotsComponent', () => {
  let component: AuctionLotsComponent;
  let fixture: ComponentFixture<AuctionLotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuctionLotsComponent ],
      imports: [
        RouterTestingModule,
        SharedModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              lotsAndAuction: {
                lots: [],
                auction: {},
              }
            }),
            params: of({
              sessionId: 2,
            })
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionLotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
