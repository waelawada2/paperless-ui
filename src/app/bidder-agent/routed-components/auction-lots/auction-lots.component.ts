import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BidLot, Auction } from '@paperless/shared';

@Component({
  selector: 'app-auction-lots',
  templateUrl: './auction-lots.component.html',
  styleUrls: ['./auction-lots.component.scss']
})
export class AuctionLotsComponent implements OnInit {

  lots: BidLot[] = [];
  nameClientsForFilter: string[] = [];
  selectedFilter;
  auction: Auction;
  session: number;
  private initialLoadedLots: BidLot[] = [];
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(({ sessionId }) => {
      this.session = +sessionId;
    });
    this.activatedRoute.data.subscribe(data => {
      this.selectedFilter = 'All';
      const { lots, auction } = data.lotsAndAuction;
      this.initialLoadedLots = lots;
      this.lots = lots;
      this.auction = auction;
      this.nameClientsForFilter = this.getUniqueClientsFromLots(lots);
      if (this.hasOnlyOneClientAssigned()) {
          this.selectedFilter = this.lots[0].clientContact.displayName;
      }
      this.onClientChange({ value: this.selectedFilter, label: this.selectedFilter });
    });
  }

  onClientChange($event: { value: string, label: string }) {
    const { value } = $event;
    this.selectedFilter = value;
    if (value === 'All') {
      this.lots = [
        ...this.initialLoadedLots,
      ];
    } else {
      this.lots = this.initialLoadedLots.filter( lot => lot.clientContact.displayName === value );
    }
  }

  hasOnlyOneClientAssigned() {
    return this.nameClientsForFilter.length === 1;
  }

  private getUniqueClientsFromLots(lots: BidLot[]) {
    const clientNames = lots.map(lot => lot.clientContact.displayName).filter(name => !!name);
    return clientNames.unique();
  }

}
