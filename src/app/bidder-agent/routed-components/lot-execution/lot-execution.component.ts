import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BidLot, BidClient } from '@paperless/shared';
import { LotsListComponent } from '../lots-list/lots-list.component';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';
import { LotService } from '@paperless/shared/services';
import { SessionService } from '@paperless/shared/security';

@Component({
  selector: 'app-lot-execution',
  templateUrl: './lot-execution.component.html',
  styleUrls: ['./lot-execution.component.scss']
})
export class LotExecutionComponent implements OnInit {

  lots: BidLot[] | null = []; // null on error
  lot$: Observable<BidLot | {}>;
  client: BidClient;

  @Output() lotWin = new EventEmitter<BidLot>();
  @Output() lotUnderbid = new EventEmitter<BidLot>();
  @Output() lotNoBid = new EventEmitter<BidLot>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private lotsListCmp: LotsListComponent,
    private lotService: LotService,
    private toasterService: ToastrService,
    private sessionService: SessionService) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data) => {
      this.lots = data.lots
      this.client = data.client;
    });
    // Suscribe to lot execution actions
    this.lotsListCmp.lotExecutionQueue$.subscribe((execution) => {
      this.onLotAction(execution);
    });
  }

  onLotAction($event: { lot: BidLot, action: string, auctionId: string, session: number, payload?: any }) {
    const { action, lot, payload, auctionId, session } = $event;
    const actionForToast = action === 'won' ? 'Won' : (action === 'underbid' ? 'Underbid' : 'No Bid');
    let actionPipeline$: Observable<Partial<BidLot>>;
          switch (action) {
            case 'won':
              actionPipeline$ = this.lotService.setLotAsWon(this.client, payload.amount, auctionId, lot.lotId, session);
              break;
            case 'underbid':
              actionPipeline$ = this.lotService.setLotAsUnderbid(this.client, payload.amount, auctionId, lot.lotId, session);
              break;
            case 'nobid':
              actionPipeline$ =  this.lotService.setLotAsNoBid(this.client, auctionId, lot.lotId, session);
              break;
            default: throw new Error('Invalid action');
          }
    actionPipeline$
      .subscribe(
        {
          error: () => {
            this.toasterService.error('', `There was a problem confirming result for Lot ${lot.lotId}. Please try again.`);
          },
          next: (modifiedLot) => {
            this.toasterService.success('', `You have successfully confirmed the Lot ${lot.lotId} as ${actionForToast}`);
            const lots = this.lotsListCmp.loadedLots;
            const [currentLot] = lots.filter(_lot => _lot.lotId === lot.lotId);
            currentLot.lotExecutionResult = modifiedLot.lotExecutionResult;
            currentLot.lotExecutionAmount = modifiedLot.lotExecutionAmount;
            currentLot.state = modifiedLot.state;
            // Find next lot that does not have a lot execution result yet
            this.lotsListCmp.goToNextAvailableLot(
              currentLot,
            );
          }
        }
      );
  }

}
