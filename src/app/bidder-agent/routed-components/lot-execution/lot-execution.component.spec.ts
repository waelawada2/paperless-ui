import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotExecutionComponent } from './lot-execution.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ClientPartyType, SharedModule } from '@paperless/shared';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs/observable/of';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ToastrModule } from 'ngx-toastr';
import { ConfigModule } from '@paperless/shared/config';
import { SecurityTestingModule } from '@paperless/shared/security/testing';
import { LotInformationComponent, ClientPanelComponent, ClientLotsPanelComponent } from '@paperless/bidder-agent/components';
import { LotsListComponent } from '@paperless/bidder-agent/routed-components';

describe('LotExecutionComponent', () => {
  let component: LotExecutionComponent;
  let fixture: ComponentFixture<LotExecutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LotExecutionComponent,
        LotInformationComponent,
        ClientPanelComponent,
        ClientLotsPanelComponent,
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        SharedModule.forRoot(),
        ToastrModule.forRoot(),
        ConfigModule,
        SecurityTestingModule
      ],
      providers: [
        ClientPartyType,
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              lots: [],
              client: {},
            }),
            params: of({
              lotId: 1,
            })
          }
        },
        {
          provide: LotsListComponent,
          useValue: {
            lotExecutionQueue$: of({
              lot: {},
              action: 'won',
              payload: {
                amount: 2000,
              }
            }),
          },
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotExecutionComponent);
    component = fixture.componentInstance;
    component.client = {} as any;
    component.lots = [];
    component.onLotAction = () => true;
   fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
