import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-bidder-agent',
  templateUrl: './bidder-agent.component.html',
  styleUrls: ['./bidder-agent.component.scss']
})
export class BidderAgentComponent implements OnInit {

  constructor ( @Inject(DOCUMENT) private document: Document) {}

  ngOnInit() {
    this.document.title = 'BID/X';
  }

}
