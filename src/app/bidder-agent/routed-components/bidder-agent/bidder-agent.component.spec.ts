import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidderAgentComponent  } from './bidder-agent.component';
import { SecurityTestingModule } from '@paperless/shared/security/testing';
import { SharedModule } from '@paperless/shared';
import { RouterTestingModule } from '@angular/router/testing';
import { ConfigModule } from '@paperless/shared/config';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('BidderAgentComponent', () => {
  let component: BidderAgentComponent;
  let fixture: ComponentFixture<BidderAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidderAgentComponent ],
      imports: [
        SharedModule,
        SecurityTestingModule,
        ConfigModule,
        RouterTestingModule,
        HttpClientTestingModule,
        NgbModule.forRoot(),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidderAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
