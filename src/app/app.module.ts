import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Components
import { AppComponent } from './app.component';

import { SecurityModule, LogonGuard } from '@paperless/shared/security';
import { ConfigModule } from '@paperless/shared/config';
import { SharedModule } from '@paperless/shared';
import { AppDeciderComponent, PageNotFoundComponent } from '@paperless/shared/components';

const routes: Route[] = [
  {
    path: 'bidx',
    loadChildren: 'app/bidder-agent/bidder-agent.module#BidderAgentModule',
  },
  {
    path: 'hammer',
    loadChildren: 'app/auctioneers-book/auctioneers-book.module#AuctioneersBookModule',
  },
  {
    path: 'reports',
    loadChildren: 'app/reports/reports.module#ReportsModule'
  },
  {
    path: '',
    pathMatch: 'full',
    component: AppDeciderComponent,
    canActivate: [LogonGuard]
  },
  {
    path: '**',
    component: PageNotFoundComponent,
    canActivate: [LogonGuard]
  }
]

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes, {
      /**
       * Our navigation occurs when keycloak is initialized. and that is done by AppComponent
       */
      initialNavigation: false,
    }),
    ConfigModule,
    SharedModule.forRoot(),
    NgbModule.forRoot(),
    SecurityModule,
    ToastrModule.forRoot()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
