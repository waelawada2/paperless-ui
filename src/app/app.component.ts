import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { KeycloakService } from '@paperless/shared/security';
import { filter, takeUntil } from 'rxjs/operators';
import { timer } from 'rxjs/observable/timer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  currentNavigatedUrl = '';
  isShowingIndicator = false;
  constructor(private router: Router, private keycloak: KeycloakService) {

  }

  ngOnInit() {
    this.keycloak.init({
      onTokenExpired: this.onTokenExpired.bind(this),
    })
      .then(() => {
        this.router.initialNavigation();
        const keycloakInstance = this.keycloak.get();
      });

    this.router.events.pipe(
      filter(routerEvent => routerEvent instanceof NavigationStart)
    )
    .subscribe((event) => this.onRouteNavigation(event as NavigationStart));
  }

  onRouteNavigation(navigationStartEvent: NavigationStart) {
    this.currentNavigatedUrl = navigationStartEvent.url;
    /**
     * Display a loading screen if it takes more than one second to load the next route!
     */
    timer(1000, 1000)
      .pipe(
        takeUntil(
          this.router.events.pipe(
            filter(routerEvent => routerEvent instanceof NavigationEnd || routerEvent instanceof NavigationError),
            filter((routerEvent: NavigationEnd) => routerEvent.url === this.currentNavigatedUrl)
          )
        ),
      ).subscribe({
        complete: () => this.isShowingIndicator = false,
        next: () => this.isShowingIndicator = true
      });
  }

  onTokenExpired() {
    const keycloak = this.keycloak.get();
    /**
     * If the token expired, try to update the token, otherwise, redirect to login page
     */
    keycloak.updateToken(5)
      .error(() => {
        keycloak.login({
          prompt: 'login',
        });
      });
  }
}
