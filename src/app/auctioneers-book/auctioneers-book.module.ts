import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AuctioneersBookRoutingModule } from './auctioneers-book-routing.module';
import { SharedModule } from '@paperless/shared';
import {
  AuctioneersBookComponent,
  AuctionsListComponent,
  AuctionInfoComponent,
  AbLotInfoContainerComponent,
} from '@paperless/auctioneers-book/routed-components';
import {
  AuctionsCardsLocationsResolver,
  LotsAndAuctionResolver,
  LotInfoResolver
} from '@paperless/auctioneers-book/services';
import {
  AbLotInfoComponent,
  OpenAuctionModalComponent,
  CloseLotModalComponent,
  UnsoldLotModalComponent,
  CloseAuctionModalComponent,
} from '@paperless/auctioneers-book/components';

@NgModule({
  imports: [
    CommonModule,
    AuctioneersBookRoutingModule,
    SharedModule,
    FormsModule,
  ],
  declarations: [
    AuctioneersBookComponent,
    AbLotInfoComponent,
    AuctionsListComponent,
    AuctionInfoComponent,
    OpenAuctionModalComponent,
    CloseLotModalComponent,
    AbLotInfoContainerComponent,
    UnsoldLotModalComponent,
    CloseAuctionModalComponent
  ],
  providers: [
    AuctionsCardsLocationsResolver,
    LotsAndAuctionResolver,
    LotInfoResolver
  ],
  entryComponents: [
    CloseLotModalComponent,
    OpenAuctionModalComponent,
    UnsoldLotModalComponent,
    CloseAuctionModalComponent,
  ]
})
export class AuctioneersBookModule { }
