export { AuctionsCardsLocationsResolver } from './auctions-cards-locations.resolver';
export { LotsAndAuctionResolver } from './lots-and-auction.resolver';
export { LotInfoResolver } from './lot-info.resolver';
