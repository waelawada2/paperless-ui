import { Injectable, } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuctionService } from '@paperless/shared/services';
import { graceFail } from '@paperless/shared/rxjs/operators';

@Injectable()
export class AuctionsCardsLocationsResolver implements Resolve<any> {
    constructor(private auctionService: AuctionService) {

    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.auctionService.getAuctionsCardsAndLocations().pipe(graceFail());
    }
}
