import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { BidLot } from '@paperless/shared';
import { LotService } from '@paperless/shared/services';
import { graceFail } from '@paperless/shared/rxjs/operators';

@Injectable()
export class LotInfoResolver implements Resolve<BidLot> {
    constructor(private auctionService: LotService) {

    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const { lotId } = route.params;
        const { auctionId, sessionId } = route.parent.params;
        return this.auctionService.getLotInfoFromAuction(
            auctionId,
            sessionId,
            lotId
        )
        .pipe(graceFail({}));
    }
}
