import { Injectable, } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuctionService } from '@paperless/shared/services';
import { graceFail } from '@paperless/shared/rxjs/operators';

@Injectable()
export class LotsAndAuctionResolver implements Resolve<any> {
    constructor(private auctionService: AuctionService) {

    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const { auctionId, sessionId } = route.params;
        return this.auctionService.getAllLotsAndAuction(auctionId, sessionId).pipe(graceFail({ lots: [], auction: {} }));
    }
}
