import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { CloseLotModalComponent } from './close-lot-modal.component';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, NgForm } from '@angular/forms';
import { SharedModule } from '@paperless/shared';

describe('CloseLotModal', () => {
  let component: CloseLotModalComponent;
  let fixture: ComponentFixture<CloseLotModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloseLotModalComponent ],
      imports: [
        FormsModule,
        SharedModule,
        NgbModule.forRoot(),
      ],
      providers: [
        {
          provide: NgbActiveModal,
          useValue: {
            dismiss() { },
            close () {

            }
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseLotModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('Should validate the paddle correctly', () => {
    const validPaddles = ['L', 'L0', 'L12', 'L123', 'L1234'];
    const invalidPaddles = ['S', '18K0', '00000'];
    const createKeypadEvent = (paddle: string) => ({
      preventDefault() { },
      futureValue: paddle,
    });
    for (const invalidPaddle of invalidPaddles) {
      const keypadEvent = createKeypadEvent(invalidPaddle);
      const preventSpy = spyOn(keypadEvent, 'preventDefault');
        component.onPaddleChanged(keypadEvent);
        expect(preventSpy).toHaveBeenCalled();
    }
    for (const validPaddle of validPaddles) {
      const keypadEvent = createKeypadEvent(validPaddle);
      const preventSpy = spyOn(keypadEvent, 'preventDefault');
      component.onPaddleChanged(keypadEvent);
      expect(preventSpy).not.toHaveBeenCalled();
    }
  });
  it('Should not close the modal when the form is invalid', inject([NgbActiveModal], (modal: NgbActiveModal) => {
    const modalSpy = spyOn(modal, 'close');
    component.onSubmit({ valid: false, invalid: true } as NgForm);
    expect(modalSpy).not.toHaveBeenCalled();
  }));
});
