import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-close-lot-modal',
  templateUrl: './close-lot-modal.component.html',
  styleUrls: ['./close-lot-modal.component.scss']
})
export class CloseLotModalComponent implements OnInit {

  currencyCode = '';
  amountFromKeypad = '';
  isShowingKeypadForAmount = true;
  deleteAmountCompletely = false;
  paddleFromKeypad = '';
  deletePaddleCompletely = false;
  isShowingKeypadForPaddle = false;

  paddleNumberValidatorPattern = /(L(?=[^0]+)\d+|[^0A-Z]+)/;

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  showAmountKeypad($event: Event) {
    if (this.isShowingKeypadForAmount) {
      return;
    }
    /**
     * Avoid keypad input from focusing if the user is only
     * switching between inputs
     */
    if (this.isShowingKeypadForPaddle) {
      $event.stopPropagation();
    }
    this.isShowingKeypadForPaddle = false;
    this.isShowingKeypadForAmount = true;
    // If the user marked the paddle input as full deletion
    // Ensure that we revert that when switching to another input
    this.deleteAmountCompletely = false;
    this.deletePaddleCompletely = false;
  }

  showPaddleKeypad($event: Event) {
    if (this.isShowingKeypadForPaddle) {
      return;
    }
    /**
     * Avoid keypad input from focusing if the user is only
     * switching between inputs
     */
    if (this.isShowingKeypadForAmount) {
      $event.stopPropagation();
    }
    this.isShowingKeypadForAmount = false;
    this.isShowingKeypadForPaddle = true;

    // If the user marked the amount input as full deletion
    // Ensure that we revert that when switching to another input
    this.deletePaddleCompletely = false;
    this.deleteAmountCompletely = false;
  }

  dismissModal() {
    this.activeModal.dismiss();
  }

  onAmountChanged($event) {
    if (!this.isAmountValueValid($event.futureValue)) {
      $event.preventDefault();
      return;
    }
    this.amountFromKeypad = $event.futureValue;
  }

  onPaddleChanged($event) {
    const paddle = $event.futureValue;
    /** Only validate paddle number, if there is actually a value
     * it there is no, it means the user deleted it completely
     * and want to start over
     */
    if (paddle && !this.isPaddleValueValid(paddle)) {
      $event.preventDefault();
      return;
    }
    this.paddleFromKeypad = $event.futureValue;
  }

  onSubmit(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.activeModal.close({
      amount: this.amountFromKeypad,
      paddle: this.paddleFromKeypad,
    });
  }

  private isPaddleValueValid(value: string) {
    const baseCheck = /^L?\d*$/i.test(value) && value.indexOf('00000') === -1;
    if (!baseCheck) {
      return false;
    }
    return value.length <= 5;
  }

  private isAmountValueValid(value: string) {
    const amountMaxLength = 10;
    if (value.length > amountMaxLength) {
      return false;
    }
    if (value.indexOf('0') === 0) {
      return false;
    }
    return true;
  }

}
