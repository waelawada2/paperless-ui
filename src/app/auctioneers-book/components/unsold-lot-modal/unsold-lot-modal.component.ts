import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-unsold-lot-modal',
  templateUrl: './unsold-lot-modal.component.html',
  styleUrls: ['./unsold-lot-modal.component.scss']
})
export class UnsoldLotModalComponent implements OnInit {

  deleteAmountCompletely = false;
  amountFromKeypad = '';
  currencyCode = '';
  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  onAmountChanged($event) {
    if (!this.isAmountValueValid($event.futureValue)) {
      $event.preventDefault();
      return;
    }
    this.amountFromKeypad = $event.futureValue;
  }

  onSubmit(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.activeModal.close({
      amount: this.amountFromKeypad,
    });
  }

  dismissModal() {
    this.activeModal.dismiss();
  }

  private isAmountValueValid(value: string) {
    const amountMaxLength = 10;
    if (value.length > amountMaxLength) {
      return false;
    }
    if (value.indexOf('0') === 0) {
      return false;
    }
    return true;
  }

}
