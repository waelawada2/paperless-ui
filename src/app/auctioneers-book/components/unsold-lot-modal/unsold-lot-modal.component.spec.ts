import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnsoldLotModalComponent } from './unsold-lot-modal.component';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '@paperless/shared';
import { FormsModule } from '@angular/forms';

describe('UnsoldLotModalComponent', () => {
  let component: UnsoldLotModalComponent;
  let fixture: ComponentFixture<UnsoldLotModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnsoldLotModalComponent ],
      imports: [
        NgbModule.forRoot(),
        SharedModule,
        FormsModule,
        NgbModule.forRoot(),
      ],
      providers: [
        {
          provide: NgbActiveModal,
          useValue: {
            dismiss() { },
            close () {

            }
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnsoldLotModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
