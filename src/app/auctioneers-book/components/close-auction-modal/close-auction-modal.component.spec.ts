import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloseAuctionModalComponent } from './close-auction-modal.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

describe('CloseAuctionModalComponent', () => {
  let component: CloseAuctionModalComponent;
  let fixture: ComponentFixture<CloseAuctionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloseAuctionModalComponent ],
      imports: [
        FormsModule,
      ],
      providers: [
        {
          provide: NgbActiveModal,
          useValue: {},
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseAuctionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
