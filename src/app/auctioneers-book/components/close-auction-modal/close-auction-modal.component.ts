import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-close-auction-modal',
  templateUrl: './close-auction-modal.component.html',
  styleUrls: ['./close-auction-modal.component.scss']
})
export class CloseAuctionModalComponent implements OnInit {

  hasUserAgreed = false;
  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  dismissModal() {
    this.activeModal.dismiss();
  }

  onSubmit(ngForm: NgForm) {
    if (ngForm.invalid) {
      return;
    }
    this.activeModal.close();
  }

}
