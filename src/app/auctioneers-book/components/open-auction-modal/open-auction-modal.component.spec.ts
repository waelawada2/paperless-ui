import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenAuctionModalComponent } from './open-auction-modal.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

describe('OpenAuctionModalComponent', () => {
  let component: OpenAuctionModalComponent;
  let fixture: ComponentFixture<OpenAuctionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenAuctionModalComponent ],
      providers: [
        {
          provide: NgbActiveModal,
          useClass: class {
            dismiss() {

            }
          }
        }
      ],
      imports: [
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenAuctionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
