import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-open-auction-modal',
  templateUrl: './open-auction-modal.component.html',
  styleUrls: ['./open-auction-modal.component.scss']
})
export class OpenAuctionModalComponent implements OnInit {

  hasUserAgreed = false;
  formSubmitted = false;
  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  dismiss() {
    this.activeModal.dismiss();
  }

  tryToConfirm() {
    if (!this.hasUserAgreed) {
      this.formSubmitted = true;
      return;
    }
    this.activeModal.close();
  }

}
