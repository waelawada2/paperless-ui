import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbLotInfoComponent } from './ab-lot-info.component';
import { SharedModule } from '@paperless/shared';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigModule } from '@paperless/shared/config';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { SecurityTestingModule } from '@paperless/shared/security/testing';
import { Subscription } from 'rxjs/Subscription';

describe('AbLotInfoComponent', () => {
  let component: AbLotInfoComponent;
  let fixture: ComponentFixture<AbLotInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbLotInfoComponent ],
      imports: [
        HttpClientTestingModule,
        ConfigModule,
        SecurityTestingModule,
        SharedModule.forRoot(),
        NgbModule.forRoot(),
        ToastrModule.forRoot(),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbLotInfoComponent);
    component = fixture.componentInstance;
    component.lot = {
      absenteeBidders: [],
      telephoneBidders: [],
      state : 'OPEN',
      withdrawn: false,
    } as any;
    component.auction = {
      state: 'OPEN',
    } as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should unsubscribe from polling when destrying the component', () => {
    component.updaterSubscription = new Subscription();
    const spier = spyOn(component.updaterSubscription, 'unsubscribe');
    component.ngOnDestroy();
    expect(spier).toHaveBeenCalled();
  })
});
