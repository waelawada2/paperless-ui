import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';

import { BidLot, Auction } from '@paperless/shared';
import { LotService } from '@paperless/shared/services';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CloseLotModalComponent } from '../close-lot-modal/close-lot-modal.component';
import { SessionService } from '@paperless/shared/security';
import { switchMap, take } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs/Subscription';
import { interval } from 'rxjs/observable/interval';
import { ConfigService } from '@paperless/shared/config';
import { UnsoldLotModalComponent } from '../unsold-lot-modal/unsold-lot-modal.component';

@Component({
  selector: 'app-ab-lot-info',
  templateUrl: './ab-lot-info.component.html',
  styleUrls: ['./ab-lot-info.component.scss']
})
export class AbLotInfoComponent implements OnInit, OnChanges, OnDestroy {

  @Input() lot: BidLot;
  @Input() auction: Auction;
  @Input() auctionSession: number;
  @Output() lotClose = new EventEmitter<BidLot>();
  @Output() nextLot = new EventEmitter<{ lot: BidLot, openLot: boolean }>();

  isRefreshingLot = false;
  imageLoaded = false;
  updaterSubscription: Subscription;
  constructor(
    private lotService: LotService,
    private modalService: NgbModal,
    private sessionService: SessionService,
    private toaster: ToastrService,
    private configService: ConfigService
  ) { }

  ngOnInit() {
    if (this.lot.state !== 'CLOSED' && !this.lot.withdrawn) {
      const timeInSeconds = this.configService.getValueOf('auctioneer-lot-refresh-interval') as number || 5;
      this.updaterSubscription = interval((timeInSeconds) * 1000).subscribe(() => this.refreshLot());
    }
  }

  ngOnDestroy() {
    if (this.updaterSubscription) {
      this.updaterSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { currentValue, previousValue } = changes.lot;
    if (!changes.lot.firstChange && currentValue.imageUrl === previousValue.imageUrl) {
      return;
    }
    this.imageLoaded = false;
  }

  refreshLot() {
    if (this.isRefreshingLot || !this.lot) {
      return;
    }
    this.isRefreshingLot = true;
    this.lotService.getLotInfoFromAuction(this.auction.auctionId, this.auctionSession, this.lot.lotId)
      .subscribe({
        next: (lot) => {
          this.lot = {
            ...this.lot,
            ...lot,
          };
          this.isRefreshingLot = false;
        },
        error: () => this.isRefreshingLot = false,
      });
  }

  onImageLoaded() {
    this.imageLoaded = true;
  }

  openSoldLotModal() {
    const modalRef = this.modalService.open(CloseLotModalComponent, {
      windowClass: 'vertical-centered',
    });
    (modalRef.componentInstance as CloseLotModalComponent).currencyCode = this.lot.currency;
    modalRef.result
      .then(({ amount, paddle }) => {
        this.lotService.setLotAsSold(
                this.auction.auctionId,
                this.auctionSession,
                this.lot.lotId,
                {
                  hammerPrice: amount,
                  paddleNumber: paddle,
                },
              ).subscribe({
              error: () => this.onLotCloseError(this.lot),
              next: () => this.onSoldLotSuccess(this.lot),
          });
      });
  }

  openUnsoldLotModal() {
    const modalRef = this.modalService.open(UnsoldLotModalComponent, {
      windowClass: 'vertical-centered',
    });
    (modalRef.componentInstance as CloseLotModalComponent).currencyCode = this.lot.currency;
    modalRef.result
      .then(({ amount }) => {
        this.lotService.setLotAsUnsold(
                this.auction.auctionId,
                this.auctionSession,
                this.lot.lotId,
                {
                  hammerPrice: amount,
                  paddleNumber: '',
                },
              ).subscribe({
              error: () => this.onLotCloseError(this.lot),
              next: () => this.onUnsoldLotSuccess(this.lot),
          });
      });
  }

  /**
   *
   * @param openLot Indicates wheter the next lot must be an open lot
   */
  requestNextLot(openLot: boolean) {
    this.nextLot.next({ lot: this.lot, openLot });
  }

  private onLotCloseError(lot: BidLot) {
    this.toaster.error(`There was a problem closing Lot #${lot.lotId}. Please try again.`);
  }

  private onUnsoldLotSuccess(lot: BidLot) {
    this.onLotCloseSuccess(lot, `You have successfully closed lot #${lot.lotId} as Unsold.`);
  }

  private onSoldLotSuccess(lot: BidLot) {
    this.onLotCloseSuccess(lot, `You have successfully closed lot #${lot.lotId} as Sold.`);
  }

  private onLotCloseSuccess(lot: BidLot, message: string) {
    lot.state = 'CLOSED';
    this.toaster.success(message);
    this.lotClose.next(lot);
    this.requestNextLot(false);
  }

}
