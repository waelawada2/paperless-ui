import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionsListComponent } from './auctions-list.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs/observable/of';
import { SharedModule, Auction } from '@paperless/shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigModule } from '@paperless/shared/config';
import { ToastrModule } from 'ngx-toastr';
import { FeatureStatusService } from '@paperless/shared/services/feature-status.service';
import { SecurityTestingModule } from '@paperless/shared/security/testing';

describe('AuctionsListComponent', () => {
  let component: AuctionsListComponent;
  let fixture: ComponentFixture<AuctionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuctionsListComponent ],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        RouterTestingModule,
        ConfigModule,
        SharedModule.forRoot(),
        NgbModule.forRoot(),
        ToastrModule.forRoot(),
        HttpClientTestingModule,
        SecurityTestingModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              auctionsCardsAndLocations: {
                cards: [],
                locations: [],
              }
            }),
            queryParams: of({
              selectedAuction: '',
              selectedSession: ''
            })
          }
        },
        {
          provide: FeatureStatusService,
          useValue: {
            isFeatureEnabled(feature: string) {
              return of(true);
            }
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionsListComponent);
    component = fixture.componentInstance;
    component.displayOpenAuctionModal = () => true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should validate correctly the open auction condition', async() => {
    const mockedAuction: Partial<Auction> = {
      openAuctionWindowStarts: Date.now(),
      openAuctionWindowEnds: Date.now(),
      state: 'SCHEDULED'
    };
    component.canOpenAuction(mockedAuction as Auction)
      .subscribe( can => expect(can).toBe(true))
    mockedAuction.state = 'OPEN';
    component.canOpenAuction(mockedAuction as Auction)
      .subscribe( can => expect(can).toBe(false) );
    const dateOneDayBefore = new Date();
    dateOneDayBefore.setDate(dateOneDayBefore.getDate() - 1);
    mockedAuction.openAuctionWindowStarts = +dateOneDayBefore
    component.canOpenAuction(mockedAuction as Auction)
      .subscribe(can => expect(can).toBe(false));
    const fiveMinsAndASecondAfter = new Date();
    fiveMinsAndASecondAfter.setMinutes(fiveMinsAndASecondAfter.getMinutes() + 5);
    fiveMinsAndASecondAfter.setSeconds(fiveMinsAndASecondAfter.getSeconds() + 1);
    mockedAuction.openAuctionWindowStarts = +fiveMinsAndASecondAfter;
    component.canOpenAuction(mockedAuction as Auction)
      .subscribe(can => expect(can).toBe(false));
  });
  it('Should go to auction lots when no condition is matched', () => {
    component.navigateToAuctionLots = () => true;
    const spyOnNavigateToAuctionLots = spyOn(component, 'navigateToAuctionLots');
    const mockedAuction: Partial<Auction> = {
      state: 'OPEN'
    };
    component.handleAuctionClick(mockedAuction as Auction);
    expect(spyOnNavigateToAuctionLots).toHaveBeenCalled();
  });
  it('Should open modal when condition is matched', () => {
    const mockedAuction: Partial<Auction> = {
      openAuctionWindowEnds: Date.now(),
      openAuctionWindowStarts: Date.now(),
      state: 'SCHEDULED'
    };
    const spyOnDisplayOpenAuction = spyOn(component, 'displayOpenAuctionModal');
    component.handleAuctionClick(mockedAuction as Auction);
    expect(spyOnDisplayOpenAuction).toHaveBeenCalled();
  })
});
