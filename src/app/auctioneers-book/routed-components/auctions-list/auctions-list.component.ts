import { Component, OnInit, ViewChildren, AfterViewInit, QueryList } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AuctionCard } from '@paperless/shared';
import { AuctionCardComponent } from '@paperless/shared/components';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OpenAuctionModalComponent } from '@paperless/auctioneers-book/components';
import { AuctionService, FeatureStatusService } from '@paperless/shared/services';
import { ToastrService } from 'ngx-toastr';
import { map, switchMap } from 'rxjs/operators';
import { SessionService } from '@paperless/shared/security';

@Component({
  selector: 'app-auctions-list',
  templateUrl: './auctions-list.component.html',
  styleUrls: ['./auctions-list.component.scss']
})
export class AuctionsListComponent implements OnInit, AfterViewInit {

  @ViewChildren(AuctionCardComponent) auctionCards: QueryList<AuctionCardComponent>;

  selectedAuction?: { id: string, session: string };
  locations: string[] = [];
  publicAuctionCards: AuctionCard[] = [];
  private _auctionsCards: AuctionCard[];
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private auctionService: AuctionService,
    private ngbModal: NgbModal,
    private toaster: ToastrService,
    private featureStatus: FeatureStatusService,
    private sessionService: SessionService) { }

  ngAfterViewInit(): void {
    if (this.selectedAuction) {
      const { id, session } = this.selectedAuction;
      const result = this.auctionCards.filter((cmp) => cmp.auction.auctionId === id);
      let auctionCmp: AuctionCardComponent;
      if (!result.length) {
        return;
      }
      if (result.length > 1) {
        [auctionCmp] = result.filter((cmp) => cmp.auction.session === +session);
      } else {
        [auctionCmp] = result;
      }
      const nativeElement = auctionCmp.elementRef.nativeElement;
      if ((nativeElement as Element).scrollIntoView) {
        nativeElement.scrollIntoView();
      }
    }
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data) => {
      const { cards, locations } = data.auctionsCardsAndLocations;
      this._auctionsCards = this.publicAuctionCards = cards;
      this.locations = locations;
    });
    this.activatedRoute.queryParams.subscribe((queryParams) => {
      this.selectedAuction = {
        id: queryParams.selectedAuction,
        session: queryParams.auctionSession === '0' ? null : queryParams.auctionSession,
      };
    });
  }

  onLocationFilterChange(location: string) {
    if (location === 'All') {
      this.publicAuctionCards = [...this._auctionsCards];
    } else {
      this.publicAuctionCards = this._auctionsCards.filter(auction => auction.location === location)
    }
  }

  handleAuctionClick(auction: AuctionCard) {
    // If auction is already opened, not need to check
    // Redirect the user to the next screen
    if (auction.state === 'OPEN') {
      this.navigateToAuctionLots(auction);
      return;
    }
    this.canOpenAuction(auction)
      .subscribe(can => {
        if (can) {
          this.selectedAuction = {
            id: auction.auctionId,
            session: auction.session as any,
          };
          this.displayOpenAuctionModal(auction);
        } else {
          this.navigateToAuctionLots(auction);
        }
      });
  }

  displayOpenAuctionModal(auction: AuctionCard) {
    this.ngbModal.open(OpenAuctionModalComponent, {
      windowClass: 'vertical-centered',
    }).result.then(() => {
      this.auctionService.openAuctionSession(auction, Date.now())
        .subscribe({
          error: () => {
            let errorMsg = `There was a problem opening the auction ${auction.auctionId}`;
            if (auction.auctionSessions > 1) {
              errorMsg += ` Session ${auction.session}`;
            }
            this.toaster.error(`${errorMsg}. Please try again.`);
          },
          next: () => {
            let successMsg = `You have successfully opened the auction ${auction.auctionId}`;
            if (auction.auctionSessions > 1) {
              successMsg += ` Session ${auction.session}`;
            }
            this.toaster.success(`${successMsg}.`);
            this.navigateToAuctionLots(auction);
          }
        });
    })
      .catch(() => void (0));
  }

  navigateToAuctionLots(auction: AuctionCard, extras?: NavigationExtras) {
    const lotsRangeRegex = /(\d+)\s+\-\s+\d+/;
    let lotId;
    /**
     * auction.lots may have the following format:
     * - <number>
     * - <number> - <number>
     * If it is a single number, we always select 1, since it is the first lot..
     * if it is a range (<number> - <number> we select the number on the left)
     */
    if (auction.lots.match(lotsRangeRegex)) {
      const result = lotsRangeRegex.exec(auction.lots);
      lotId = result[1];
    } else {
      lotId = 1;
    }
    this.router.navigate(['./auction', auction.auctionId, 'session', auction.session, 'lot', lotId], {
      relativeTo: this.activatedRoute,
      ...extras,
    });
  }

  canOpenAuction(auction: AuctionCard) {
    const currentDate = Date.now();
    const canOpen = auction.state === 'SCHEDULED';
    const isValid = (
      auction.openAuctionWindowStarts <= currentDate
      && currentDate <= auction.openAuctionWindowEnds
    );
    return this.featureStatus.isFeatureEnabled('VALIDATE_OPEN_AUCTION_TIME_WINDOW')
      .pipe(
        map(e => (!e && canOpen) || (isValid && canOpen))
      );
  }

}
