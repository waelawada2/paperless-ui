import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionInfoComponent } from './auction-info.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AbLotInfoComponent } from '@paperless/auctioneers-book/components';
import { SharedModule } from '@paperless/shared';
import { ActivatedRoute } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigModule } from '@paperless/shared/config';
import { SecurityTestingModule } from '@paperless/shared/security/testing';
import { of } from 'rxjs/observable/of';

describe('AuctionInfoComponent', () => {
  let component: AuctionInfoComponent;
  let fixture: ComponentFixture<AuctionInfoComponent>;

  beforeEach(async(() => {
    const activatedRouteData = {
      lotsAndAuction: {
        lots: [],
        auction: {},
      }
    };
    TestBed.configureTestingModule({
      declarations: [
        AuctionInfoComponent,
        AbLotInfoComponent,
      ],
      imports: [
        RouterTestingModule,
        SharedModule.forRoot(),
        NgbModule.forRoot(),
        ToastrModule.forRoot(),
        ConfigModule,
        HttpClientTestingModule,
        SecurityTestingModule,

      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of(activatedRouteData),
            snapshot: {
              data: activatedRouteData,
              params: {
                sessionId: 2,
              },
              queryParams: {
                selectedLot: '1',
                selectFirst: false,
              },
            }
          }
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionInfoComponent);
    component = fixture.componentInstance;
    component.auction = {} as any;
    component.resolvedLots = [
      {
        state: 'OPEN',
        withdrawn: false,
      },
      {
        state: 'CLOSED',
        withdrawn: false,
      }
    ] as any;
    component.selectedLot = {} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should enable the close button when all lots are closed', () => {
    component.resolvedLots = [
      {
        state: 'CLOSED',
        withdrawn: false,
      },
      {
        state: 'CLOSED',
        withdrawn: false,
      },
      {
        state: 'OPEN',
        withdrawn: true,
      }
    ] as any;
    fixture.detectChanges();
    const closeDiv = (fixture.nativeElement as HTMLElement).querySelector('.auction-info__close-auction--disabled');
    expect(closeDiv).toBeNull();
  });
});
