import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { BidLot, AuctionCard } from '@paperless/shared';
import { ActivatedRoute, Router } from '@angular/router';
import { LotCardComponent } from '@paperless/shared/components';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CloseAuctionModalComponent } from '@paperless/auctioneers-book/components';
import { AuctionService, LoadingScreenService } from '@paperless/shared/services';
import { ToastrService } from 'ngx-toastr';
import { SessionService } from '@paperless/shared/security';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-auction-info',
  templateUrl: './auction-info.component.html',
  styleUrls: ['./auction-info.component.scss']
})
export class AuctionInfoComponent implements OnInit {

  @ViewChildren(LotCardComponent) lotCardsCmp: QueryList<LotCardComponent>;
  auction: AuctionCard;
  resolvedLots: BidLot[];
  session: number;
  allLotsAreClosed = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private toasterService: ToastrService,
    private auctionService: AuctionService,
    private sessionService: SessionService,
    private loadingScrenService: LoadingScreenService) { }

  ngOnInit() {
    // Get resolved data from router
    const { lots, auction } = this.activatedRoute.snapshot.data.lotsAndAuction;
    this.session = +this.activatedRoute.snapshot.params.sessionId;

    this.auction = { ...auction, session: this.session };
    this.resolvedLots = lots;

    this.revalidateAllLotsCloseProperty();
  }

  onLotClose(lot: BidLot) {
    const [affectedLot] = this.resolvedLots.filter(_lot => _lot.lotId === lot.lotId);
    affectedLot.state = lot.state;
    this.revalidateAllLotsCloseProperty();
  }

  onNextLot($event: { lot: BidLot, openLot: boolean }) {
    const { lot, openLot } = $event;
    const [affectedLot] = this.resolvedLots.filter(_lot => _lot.lotId === lot.lotId);
    const nextLot = openLot ? this.findNextOpenLotRelativeTo(affectedLot) : this.findNextLotRelativeTo(lot);
    if (nextLot) {
      this.navigateToLot(nextLot);
      this.scrollLotCardComponentIntoView(this.getLotCardComponentByLotId(nextLot.lotId));
    }
  }

  openAuctionCloseModal() {
    const modalRef = this.modalService.open(CloseAuctionModalComponent, {
      windowClass: 'vertical-centered'
    });
    modalRef.result.then(() => {
      this.auctionService.closeAuctionSession(this.auction)
      .pipe(
        this.loadingScrenService.asRxJsOperator()
      )
      .subscribe({
        next: () => this.onAuctionCloseSuccess(this.auction),
        error: () => this.onAuctionCloseError(),
      });
    });
  }

  private onAuctionCloseSuccess(auction: AuctionCard) {
    let message = `You have successfully closed the auction ${this.auction.auctionId}`;
    if (auction.auctionSessions > 1) {
      message += ` Session ${this.session}`;
    }
    message += '.';
    this.toasterService.success(
      message,
    );
    this.router.navigate(['/hammer'], {
      queryParams: {
        selectedAuction: this.auction.auctionId,
        auctionSession: this.session,
      },
    });
  }

  private onAuctionCloseError() {
    let message = `There was a problem closing the auction ${this.auction.auctionId}`
    if (this.auction.auctionSessions > 1) {
      message += ` Session ${this.session}`;
    }
    message += '. Please try again.';
    this.toasterService.error(
     message,
    );
  }

  private revalidateAllLotsCloseProperty() {
    this.allLotsAreClosed = this.resolvedLots.every(lot => lot.state === 'CLOSED' || !!lot.withdrawn);
  }

  private getLotCardComponentByLotId(lotId: string) {
    const [matched] = this.lotCardsCmp.filter(cmp => cmp.lot.lotId === lotId);
    return matched;
  }

  private scrollLotCardComponentIntoView(lotCard: LotCardComponent) {
    const nativeElement = lotCard.elementRef.nativeElement;
    if (nativeElement.scrollIntoView) {
      nativeElement.scrollIntoView();
    }
  }

  private findNextLotRelativeTo(lot: BidLot) {
    const matcher = (_lot: BidLot) => _lot.lotId !== lot.lotId;
    return this.findNextLotByCondition(matcher, lot);
  }

  private findNextOpenLotRelativeTo(lot: BidLot) {
    const matcher = _lot => _lot.state !== 'CLOSED' && _lot.state !== 'SCHEDULED' && !_lot.withdrawn;
    return this.findNextLotByCondition(matcher, lot);
  }

  private findNextLotByCondition(matcher: (lot: BidLot) => boolean, lot: BidLot) {
    const [matchingLot] = this.resolvedLots.filter(_lot => _lot.lotId === lot.lotId);
    const currentLotindex = this.resolvedLots.indexOf(matchingLot);
    // Search on the right side of the array for a lot that matches the given criteria
    const rightSection = this.resolvedLots.slice(currentLotindex + 1, this.resolvedLots.length);
    let [result] = rightSection.filter(matcher);
    if (result) {
      return result;
    } else {
      // IF we could not find it in the right side, we look for it on the left side
      const leftSection = this.resolvedLots.slice(0, currentLotindex);
      [result] = leftSection.filter(matcher);
      return result;
    }
  }

  private navigateToLot(lot: BidLot) {
    this.router.navigate(['lot', lot.lotId], {
      relativeTo: this.activatedRoute,
      queryParams: {
        selectedLot: lot.lotId,
      }
    });
  }

}
