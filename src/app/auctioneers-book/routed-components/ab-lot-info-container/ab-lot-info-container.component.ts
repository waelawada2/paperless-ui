import { Component, OnInit, Host, OnDestroy } from '@angular/core';
import { AuctionInfoComponent } from '../auction-info/auction-info.component';
import { BidLot, AuctionCard } from '@paperless/shared';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-ab-lot-info-container',
  templateUrl: './ab-lot-info-container.component.html',
})
export class AbLotInfoContainerComponent implements OnInit, OnDestroy {

  auction: AuctionCard;
  auctionSession: number;
  resolvedLot: BidLot;

  private dataRouteSubscription: Subscription;
  constructor(
    @Host() private auctionInfoCmp: AuctionInfoComponent,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.dataRouteSubscription = this.activatedRoute.data.subscribe(data => {
      this.resolvedLot = data.lot;
    });
    this.auction = this.auctionInfoCmp.auction;
    this.auctionSession = this.auctionInfoCmp.session;
  }

  onLotClose($event: BidLot) {
    this.auctionInfoCmp.onLotClose($event);
  }

  onNextLot($event: { lot: BidLot, openLot: boolean }) {
    this.auctionInfoCmp.onNextLot($event);
  }

  ngOnDestroy() {
    this.dataRouteSubscription.unsubscribe();
  }

}
