import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbLotInfoContainerComponent } from './ab-lot-info-container.component';
import { AbLotInfoComponent } from '@paperless/auctioneers-book/components';
import { SharedModule, BidLot } from '@paperless/shared';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs/observable/of';
import { AuctionInfoComponent } from '@paperless/auctioneers-book/routed-components';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigModule } from '@paperless/shared/config';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SecurityTestingModule } from '@paperless/shared/security/testing';
import { ToastrModule } from 'ngx-toastr';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('AbLotInfoContainerComponent', () => {
  let component: AbLotInfoContainerComponent;
  let fixture: ComponentFixture<AbLotInfoContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbLotInfoContainerComponent, AbLotInfoComponent ],
      imports: [
        SharedModule.forRoot(),
        HttpClientTestingModule,
        ConfigModule,
        NgbModule.forRoot(),
        SecurityTestingModule,
        NoopAnimationsModule,
        ToastrModule.forRoot(),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              lot: {
                telephoneBidders: [],
              }
            })
          }
        },
        {
          provide: AuctionInfoComponent,
          useValue: {
            auction: {},
            session: 1,
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbLotInfoContainerComponent);
    component = fixture.componentInstance;
    component.resolvedLot = {
      absenteeBidders: [],
    } as BidLot;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
