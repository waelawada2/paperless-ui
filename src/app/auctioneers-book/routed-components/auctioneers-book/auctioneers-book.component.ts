import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-auctioneers-book',
  templateUrl: './auctioneers-book.component.html',
  styleUrls: ['./auctioneers-book.component.scss']
})
export class AuctioneersBookComponent implements OnInit {

  constructor ( @Inject(DOCUMENT) private document: Document) {}

  ngOnInit() {
    this.document.title = 'HAMMER';
  }

}
