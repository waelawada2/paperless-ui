import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AuctioneersBookComponent } from './auctioneers-book.component';
import { SharedModule } from '@paperless/shared';
import { AuctionService } from '@paperless/shared/services';
import { ConfigModule } from '@paperless/shared/config';
import { AbLotInfoComponent } from '@paperless/auctioneers-book/components';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SecurityTestingModule } from '@paperless/shared/security/testing';

describe('AuctioneersBookComponent', () => {
  let component: AuctioneersBookComponent;
  let fixture: ComponentFixture<AuctioneersBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuctioneersBookComponent, AbLotInfoComponent ],
      imports: [
        SecurityTestingModule,
        SharedModule.forRoot(),
        HttpClientTestingModule,
        ConfigModule,
        RouterTestingModule,
        NgbModule.forRoot(),
      ],
      providers: [
        AuctionService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctioneersBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
