import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogonGuard, HammerRoleGuard } from '@paperless/shared/security';
import {
  AuctioneersBookComponent,
  AuctionsListComponent,
  AuctionInfoComponent,
  AbLotInfoContainerComponent,
} from '@paperless/auctioneers-book/routed-components';
import {
  AuctionsCardsLocationsResolver,
  LotsAndAuctionResolver,
  LotInfoResolver,
} from '@paperless/auctioneers-book/services';

const routes: Routes = [{
  path: '',
  component: AuctioneersBookComponent,
  canActivate: [LogonGuard, HammerRoleGuard],
  children: [
    {
      path: '',
      pathMatch: 'full',
      component: AuctionsListComponent,
      resolve: {
        auctionsCardsAndLocations: AuctionsCardsLocationsResolver,
      }
    },
    {
      path: 'auction/:auctionId/session/:sessionId',
      component:  AuctionInfoComponent,
      resolve: {
        lotsAndAuction: LotsAndAuctionResolver,
      },
      children: [
        {
          path: 'lot/:lotId',
          component: AbLotInfoContainerComponent,
          resolve: {
            lot: LotInfoResolver,
          }
        }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuctioneersBookRoutingModule { }
