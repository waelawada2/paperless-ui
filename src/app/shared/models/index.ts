export { BidClient } from './bid-client.interface';
export { BidLot } from './bid-lot.interface';
export { Auction, AuctionCard, AuctionState } from './auction.interface';
export { Location } from './location.interface';
export { ResolveResult } from './resolve-result.interface';
