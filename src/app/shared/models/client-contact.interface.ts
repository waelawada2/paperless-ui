export interface ClientContact {
    clientNumber: string,
    displayName: string
}
