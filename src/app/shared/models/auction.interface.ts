export interface Auction {
    auctionId: string,
    auctionName: string,
    session: number,
    openAuctionWindowStarts: number,
    openAuctionWindowEnds: number,
    lots: string,
    location: string,
    state: AuctionState,
    date: string;
    time: string;
}

export interface AuctionCard extends Auction {
    auctionSessions: number;
}

export type AuctionState = 'OPEN' | 'SCHEDULED' | 'CLOSED';
