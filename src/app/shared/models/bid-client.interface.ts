export interface BidClient {
    clientNumber: number;
    clientLevel: number;
    paddle: string;
    partyName: string;
    firstName: string;
    lastName: string;
    kcm: string;
    partyType: string;
    languagePreference: string;
    preferredPhone: string;
    otherPhone: string;
    email: string;
    coverBid: number;
    coverBidCurrency: string;
}
