export interface Location {
    path: string;
    name: string;
}
