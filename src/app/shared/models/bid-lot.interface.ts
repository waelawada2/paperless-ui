import { ClientContact } from './client-contact.interface';

export interface BidLot {
    lotId: string;
    lotAuthor: string;
    lotDescription: string;
    estimateLow: number;
    estimateHigh: number;
    currency: string;
    imageUrl: string;
    clientContact?: ClientContact;
    medium: string;
    executed: string;
    reservePrice: number;
    lotExecutionResult: 'WON' | 'NOBID' | 'UNDERBID';
    withdrawn?: boolean;
    state?: 'CONFIRMED_BY_BIDDER' | 'OPEN' | 'SCHEDULED' | 'CLOSED' | 'PENDING';
    lotExecutionAmount?: number;
    lotClosureResult: 'SLD' | 'BI';
    lotClosureAmount?: number;
    lotClosurePaddle: string;
    absenteeBidders?: Array<{ clientNumber: number; paddle: string; amount: number; isReserve: boolean}>;
    telephoneBidders?: Array<{ userName: string; name: string; paddleNumber: string }>;
}
