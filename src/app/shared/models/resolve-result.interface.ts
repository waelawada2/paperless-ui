export interface ResolveResult {
    hadErrors: boolean;
    error?: any;
    [key: string]: any;
}
