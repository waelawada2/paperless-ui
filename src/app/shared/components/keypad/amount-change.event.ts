export class AmountChangedEvent {
    private _prevented = false;

    constructor(
        public currentValue: string,
        public futureValue: string) {

    }

    wasPreventedDefault() {
        return this._prevented;
    }

    preventDefault() {
        this._prevented = true;
    }

}
