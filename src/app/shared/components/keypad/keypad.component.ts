import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { AmountChangedEvent } from './amount-change.event';

@Component({
  selector: 'app-keypad',
  templateUrl: './keypad.component.html',
  styleUrls: ['./keypad.component.scss']
})
export class KeypadComponent implements OnInit {

  @Input() set fullDeletion(val: boolean) {
    this._isMarkedForFullDeletion = val;
  }
  @Input() keypadFor: 'bidder' | 'auctioner' = 'bidder';
  @Input() initialValue;
  @Output() amountChanged = new EventEmitter<AmountChangedEvent>();
  @Output() fullDeletionChange = new EventEmitter<boolean>();

  currentAmount = '';
  isSubmitted = false;
  hasErrors = true;

  private _isMarkedForFullDeletion = false;
  constructor() { }

  ngOnInit() {
    if (this.initialValue) {
      this.currentAmount = this.initialValue;
    }
  }

  markForFullDeletion() {
    this._isMarkedForFullDeletion = true;
  }

  append(number: string) {
    this.tryToChangeAmount(`${this.currentAmount}${number}`);
  }

  removeLastUnit() {
    const result = this.currentAmount.substring(0, this.currentAmount.length - 1);
    this.tryToChangeAmount(result);
  }


  handleDelete() {
    if (this._isMarkedForFullDeletion) {
      this.tryToChangeAmount('');
      this._isMarkedForFullDeletion = false;
    } else {
      this.removeLastUnit();
    }
    this.fullDeletionChange.emit(false);
  }

  private tryToChangeAmount(futureAmount: string) {
    const shouldCommitAmount = !this._onAmountChange(futureAmount);
    if (shouldCommitAmount) {
      this.currentAmount = futureAmount;
    }
  }

  private _onAmountChange(futureAmount: string) {
    const amountChangeEvent = new AmountChangedEvent(this.currentAmount, futureAmount)
    this.amountChanged.emit(amountChangeEvent);
    return amountChangeEvent.wasPreventedDefault();
  }

}
