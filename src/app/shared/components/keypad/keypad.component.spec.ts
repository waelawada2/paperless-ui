import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeypadComponent } from './keypad.component';

describe('KeypadComponent', () => {
  let component: KeypadComponent;
  let fixture: ComponentFixture<KeypadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeypadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeypadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should concat numbers correctly', () => {
    component.append('1');
    component.append('3');
    expect(component.currentAmount).toBe('13');
    component.append('0');
    expect(component.currentAmount).toBe('130');
    component.handleDelete();
    expect(component.currentAmount).toBe('13');
  });
  it('should handle delete key correctly', () => {
    component.append('2');
    component.append('000');
    component.handleDelete();
    expect(component.currentAmount).toBe('200');
    component.markForFullDeletion();
    component.handleDelete();
    expect(component.currentAmount).toBe('');
    component.append('2');
    component.append('00');
    component.handleDelete();
    expect(component.currentAmount).toBe('20');
  });
});
