import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarComponent } from './navbar.component';
import { GetUserImageUrlPipe } from '@paperless/shared/pipes/get-image-url.pipe';
import { SessionService } from '@paperless/shared/security';
import { ConfigModule } from '@paperless/shared/config';
import { of } from 'rxjs/observable/of';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserImageComponent } from '@paperless/shared/components';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarComponent, GetUserImageUrlPipe, UserImageComponent ],
      imports: [
        ConfigModule,
        HttpClientTestingModule,
      ],
      providers: [
        {
          provide: SessionService,
          useClass: class {
            get userProfile$() {
              return of({});
            }
          }
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
