import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { SessionService } from '@paperless/shared/security';
import { map } from 'rxjs/operators';
import { ConfigService } from '@paperless/shared/config';

declare const window: any;
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Input() appName = '';
  currentUserName$ = this.sessionService.userProfile$.pipe(map((user) => user.username));
  currentUserFullName$ = this.sessionService.userProfile$.pipe(map(user => `${user.firstName} ${user.lastName}`));

  constructor(private sessionService: SessionService, private configService: ConfigService) { }

  ngOnInit() {
    this.sessionService.userProfile$.subscribe((user) => {
      console.log(user);
      // If this is not a production build, create a function for the global context
      // In order to retrieve user details (QA)
      if (!this.configService.getValueOf('production')) {
        window.getCurrentLoggedUser = () => user;
      }
    });
  }

  logout() {
    this.sessionService.logout();
  }

}
