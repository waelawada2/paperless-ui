import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { SbDropdownComponent } from '../sb-dropdown/sb-dropdown.component';

@Component({
  selector: 'app-sb-dropdown-option',
  templateUrl: './sb-dropdown-option.component.html',
  styleUrls: ['./sb-dropdown-option.component.scss']
})
export class SbDropdownOptionComponent implements OnInit, OnDestroy {

  @Input() value: any;
  @Input() label: string;

  selected = false;

  private hostCmp: SbDropdownComponent;
  constructor() { }

  ngOnInit() {
  }

  registerHost(hostCmp: SbDropdownComponent) {
    this.hostCmp = hostCmp;
  }

  selectOption() {
    this.hostCmp.selectOption(this);
  }

  ngOnDestroy(): void {
   this.hostCmp = null;
  }

}
