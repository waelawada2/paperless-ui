import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SbDropdownOptionComponent } from './sb-dropdown-option.component';

describe('SbDropdownOptionComponent', () => {
  let component: SbDropdownOptionComponent;
  let fixture: ComponentFixture<SbDropdownOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SbDropdownOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SbDropdownOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
