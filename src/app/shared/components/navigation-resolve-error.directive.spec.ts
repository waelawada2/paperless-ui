import { NavigationResolveErrorDirective } from './navigation-resolve-error.directive';
import { Component } from '@angular/core';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { SharedModule, ResolveResult } from '@paperless/shared';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';

@Component({
  template: `
    <div *unlessNavigationResolveError="{ checkFor: 'test' }">
      I should be rendered if success
    </div>
  `
})
// tslint:disable-next-line:component-class-suffix
class ResolveDirectiveComponentTest {

}


describe('NavigationResolveErrorDirective', () => {
  let component: ResolveDirectiveComponentTest;
  let fixture: ComponentFixture<ResolveDirectiveComponentTest>;
  let dataSubject = new Subject<{ [key: string]: ResolveResult }>();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolveDirectiveComponentTest ],
      imports: [
        SharedModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: dataSubject.asObservable(),
          }
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolveDirectiveComponentTest);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Should display the text correctly when the route does not fail', () => {
    dataSubject.next({
      test: {
        hadErrors: false,
      }
    });
    const divElement = fixture.nativeElement as HTMLElement;
    const innerText = divElement.innerText;
    expect(innerText).toContain('I should be');
  });

  it('Should not display the text correctly when the route fails', () => {
    dataSubject.next({
      test: {
        hadErrors: true,
      }
    });
    const divElement = fixture.nativeElement as HTMLElement;
    const innerText = divElement.innerText;
    expect(innerText).not.toContain('I should be');
  });
});
