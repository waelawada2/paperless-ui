import {
  Component,
  OnInit,
  Input,
  ContentChild,
  TemplateRef,
  forwardRef,
  Host,
  Output,
  EventEmitter,
  Optional
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgForm } from '@angular/forms';

@Component({
  selector: 'app-keypad-input',
  templateUrl: './keypad-input.component.html',
  styleUrls: ['./keypad-input.component.scss'],
  providers: [{
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => KeypadInputComponent),
    }
  ]
})
export class KeypadInputComponent implements OnInit, ControlValueAccessor {

  @Input()
  emptyLabelTemplate?: TemplateRef<any>;
  @Input()
  confirmationLabel = '';
  @Input()
  errorTemplate?: TemplateRef<any>;
  @Input()
  isFocused = false;
  @Input()
  isActive = true;
  @Input()
  suffix = '';
  @Output()
  isFocusedChange = new EventEmitter<boolean>();
  @ContentChild(TemplateRef) valueTemplate?: TemplateRef<any>;
  amount: string;
  valueContext = {
    amount: '',
  }
  templateContext = {
    $implicit: this.valueContext,
  };
  isDisabled = false;
  submitted = false;
  // context for template
  onChange: (amount: any) => any;
  onTouched: () => any;

  constructor(@Optional() @Host() public parentForm: NgForm) {
  }

  ngOnInit() {
  }
  focusInput() {
    this.isFocusedChange.emit(true);
  }
  writeValue(obj: any): void {
    this.amount = obj;
    this.valueContext.amount = obj;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

}
