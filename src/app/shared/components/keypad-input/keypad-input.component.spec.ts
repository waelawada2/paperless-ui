import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeypadInputComponent } from './keypad-input.component';

describe('KeypadInputComponent', () => {
  let component: KeypadInputComponent;
  let fixture: ComponentFixture<KeypadInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeypadInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeypadInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
