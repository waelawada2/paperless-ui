import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SecurityTestingModule } from '@paperless/shared/security/testing';
import { ConfigModule } from '@paperless/shared/config';

import { AppDeciderComponent } from './app-decider.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SessionService } from '@paperless/shared/security';

describe('AppDeciderComponent', () => {
  let component: AppDeciderComponent;
  let fixture: ComponentFixture<AppDeciderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppDeciderComponent ],
      imports: [
        RouterTestingModule,
        SecurityTestingModule,
        ConfigModule,
        HttpClientTestingModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppDeciderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Test redirection to hammer app', () => {
    beforeEach(inject([SessionService], (sessionService: SessionService) => {
      spyOn(sessionService, 'hasHammerRole').and.returnValue(true);
      spyOn(sessionService, 'hasBidxRole').and.returnValue(false);
      spyOn(sessionService, 'hasReportsRole').and.returnValue(false);
    }));
    it('should redirect the user to hammer when the user has that role', () => {
      const spier = spyOn(component, 'redirectUserToHammer');
      component.ngOnInit();
      expect(spier).toHaveBeenCalled();
    });
  });
  describe('Test redirection to bidx app', () => {
    beforeEach(inject([SessionService], (sessionService: SessionService) => {
      spyOn(sessionService, 'hasBidxRole').and.returnValue(true);
      spyOn(sessionService, 'hasHammerRole').and.returnValue(false);
      spyOn(sessionService, 'hasReportsRole').and.returnValue(false);
    }));
    it('should redirect the user to bidx when the user has that role', () => {
      const spier = spyOn(component, 'redirectUserToBidx');
      component.ngOnInit();
      expect(spier).toHaveBeenCalled();
    });
  });
  describe('Test redirection to reports app', () => {
    beforeEach(inject([SessionService], (sessionService: SessionService) => {
      spyOn(sessionService, 'hasBidxRole').and.returnValue(false);
      spyOn(sessionService, 'hasHammerRole').and.returnValue(false);
      spyOn(sessionService, 'hasReportsRole').and.returnValue(true);
    }));
    it('should redirect the user to reports when the user has that role', () => {
      const spier = spyOn(component, 'redirectUserToReports');
      component.ngOnInit();
      expect(spier).toHaveBeenCalled();
    });
  });
  describe('Test users ability to choose the app he wants to go', () => {
    beforeEach(inject([SessionService], (sessionService: SessionService) => {
      spyOn(sessionService, 'hasReportsRole').and.returnValue(false);
      spyOn(sessionService, 'hasBidxRole').and.returnValue(true);
      spyOn(sessionService, 'hasHammerRole').and.returnValue(true);
    }));
    it('It should not redirect the user', () => {
      const spierOnReports = spyOn(component, 'redirectUserToReports');
      const spierOnBidx = spyOn(component, 'redirectUserToBidx');
      const spierOnHammer = spyOn(component, 'redirectUserToHammer');
      component.ngOnInit();
      expect(spierOnBidx).not.toHaveBeenCalled();
      expect(spierOnReports).not.toHaveBeenCalled();
      expect(spierOnHammer).not.toHaveBeenCalled();
    });
  });
});
