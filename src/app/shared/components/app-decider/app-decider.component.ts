import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from '@paperless/shared/security';

@Component({
  selector: 'app-app-decider',
  templateUrl: './app-decider.component.html',
  styleUrls: ['./app-decider.component.scss']
})
export class AppDeciderComponent implements OnInit {

  constructor(public router: Router, private sessionService: SessionService) { }

  ngOnInit() {
    this.decideWhatToDo();
  }

  hasUserAccessToBidx(): 1 | 0 {
    return this.sessionService.hasBidxRole() ? 1 : 0;
  }

  hasUserAccessToHammer(): 1 | 0 {
    return this.sessionService.hasHammerRole() ? 1 : 0;
  }

  hasUserAccessToReports(): 1 | 0 {
    return this.sessionService.hasReportsRole() ? 1 : 0;
  }

  HasUserAccessToMoreThanOneApp() {
    const accessToBidx = this.hasUserAccessToBidx(),
      accessToHammer = this.hasUserAccessToHammer(),
      accessToReports = this.hasUserAccessToReports();
    return accessToBidx + accessToHammer + accessToReports >= 2;
  }

  redirectUserToHammer() {
    this.redirectUserByUrl('/hammer');
  }

  redirectUserToBidx() {
    this.redirectUserByUrl('/bidx/auctions');
  }

  redirectUserToReports() {
    this.redirectUserByUrl('/reports');
  }

  private decideWhatToDo() {
    if (!this.HasUserAccessToMoreThanOneApp()) {
      if (this.hasUserAccessToBidx()) {
        this.redirectUserToBidx();
      } else if (this.hasUserAccessToHammer()) {
        this.redirectUserToHammer();
      } else {
        this.redirectUserToReports();
      }
    }
  }

  private redirectUserByUrl(url: string) {
    this.router.navigateByUrl(url);
  }


}
