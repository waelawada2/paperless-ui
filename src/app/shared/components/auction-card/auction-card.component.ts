import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { AuctionCard, AuctionState } from '@paperless/shared';

@Component({
  selector: 'app-auction-card',
  templateUrl: './auction-card.component.html',
  styleUrls: ['./auction-card.component.scss']
})
export class AuctionCardComponent implements OnInit {

  @Input() auction: AuctionCard;

  constructor(public elementRef: ElementRef) { }

  ngOnInit() {
  }

  isAuctionInState(state: AuctionState) {
    return this.auction && this.auction.state === state;
  }

  getClassByAuctionState() {
    const baseClass = `auction-card`;
    if (this.isAuctionInState('CLOSED')) {
      return `${baseClass}--closed`;
    } else if (this.isAuctionInState('SCHEDULED')) {
      return `${baseClass}--scheduled`;
    } else {
      return `${baseClass}--open`;
    }
  }

}
