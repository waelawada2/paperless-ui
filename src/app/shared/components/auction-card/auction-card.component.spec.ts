import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionCardComponent } from './auction-card.component';
import { CommonModule } from '@angular/common';

describe('AuctionCardComponent', () => {
  let component: AuctionCardComponent;
  let fixture: ComponentFixture<AuctionCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuctionCardComponent ],
      imports: [
        CommonModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionCardComponent);
    component = fixture.componentInstance;
    component.auction = {

    } as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get the correct class depending on state', () => {
    component.auction = {
      state: 'OPEN',
    } as any;
    expect(component.getClassByAuctionState()).toContain('--open');
    component.auction.state = 'SCHEDULED';
    expect(component.getClassByAuctionState()).toContain('--scheduled');
  });
});
