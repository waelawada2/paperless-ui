import {
  Directive,
  Input,
  TemplateRef,
  ViewContainerRef,
  ComponentFactoryResolver,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { RouterOutlet, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ResolveResult } from '@paperless/shared';
import { take } from 'rxjs/operators';
import { NavigationErrorComponent } from '@paperless/shared/components';

interface DirectiveOptions {
  checkFor: string;
}

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[unlessNavigationResolveError]'
})
export class NavigationResolveErrorDirective implements OnInit, OnDestroy {

  @Input() unlessNavigationResolveError: DirectiveOptions = { checkFor: '' };
  private subscription: Subscription;
  constructor(private activatedRoute: ActivatedRoute,
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef,
    private cfr: ComponentFactoryResolver) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.data.subscribe(result => {
      this.handleResult(result as ResolveResult);
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private handleResult(resolveResult: {[key: string]: ResolveResult}) {
    const { checkFor } = this.unlessNavigationResolveError;
    if (checkFor) {
      this.viewContainerRef.clear();
      const result = resolveResult[checkFor];
      if (!result.hadErrors) {
        this.viewContainerRef.createEmbeddedView(this.templateRef);
      } else {
        const componentFactory = this.cfr.resolveComponentFactory(NavigationErrorComponent);
        this.viewContainerRef.createComponent(componentFactory);
      }
    }
  }

}
