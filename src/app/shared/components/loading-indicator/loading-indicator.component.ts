import { Component, OnInit, Input } from '@angular/core';
import { LoadingScreenService } from '@paperless/shared/services';

@Component({
  selector: 'app-loading-indicator',
  templateUrl: './loading-indicator.component.html',
  styleUrls: ['./loading-indicator.component.scss']
})
export class LoadingIndicatorComponent implements OnInit {

  isDisplaying = false;
  private displayRequestsQueue = [];
  @Input() set isActive (value: boolean) {
    this.performQueueOperation(value);
  }

  constructor(private loadingScreen: LoadingScreenService) {

  }

  ngOnInit() {
    this.loadingScreen.loadingQueue$.subscribe((shouldDisplay) => {
      this.performQueueOperation(shouldDisplay);
    });
  }

  private performQueueOperation(value: boolean) {
    if (value) {
      this.displayRequestsQueue.push(value);
    } else {
      this.displayRequestsQueue.pop();
    }
    if (this.displayRequestsQueue.length === 0) {
      this.isDisplaying = false;
    } else {
      this.isDisplaying = true;
    }
  }


}
