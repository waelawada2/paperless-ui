import {
  Component,
  OnChanges,
  OnInit,
  QueryList,
  ContentChildren,
  Input,
  AfterContentInit,
  EventEmitter,
  Output,
  Optional,
  Attribute,
  SimpleChanges,
} from '@angular/core';
import { SbDropdownOptionComponent } from '../sb-dropdown-option/sb-dropdown-option.component';

@Component({
  selector: 'app-sb-dropdown',
  templateUrl: './sb-dropdown.component.html',
  styleUrls: ['./sb-dropdown.component.scss'],
})
export class SbDropdownComponent implements OnInit, AfterContentInit, OnChanges {

  @Input() label: string;
  @Input() selected?: any;
  @Input() disabled = false;
  @Output() optionChanged = new EventEmitter<{ value: any, label: string}>();
  @ContentChildren(SbDropdownOptionComponent) dropdownOptions: QueryList<SbDropdownOptionComponent>;

  selectedOption = null;
  width = 'auto';
  isDisabled = false;

  constructor(@Optional() @Attribute('class') public className: string) {
      if (!this.className) {
        this.className = '';
      }
    }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { selected: selectedChange } = changes;
    if (!selectedChange.isFirstChange()) {
      this.findByValueAndMarkAsSelected(selectedChange.currentValue);
    }
  }

  ngAfterContentInit() {
    this.dropdownOptions.forEach((optionCmp) => optionCmp.registerHost(this));
    this.dropdownOptions.changes.subscribe((t) => {
      this.dropdownOptions.forEach(option => option.registerHost(this));
    });
    if (this.selected) {
      this.findByValueAndMarkAsSelected(this.selected);
    }
  }


  selectOption(option: SbDropdownOptionComponent) {
    const { label, value } = option;
    this.selectedOption = {
      label,
      value,
    };
    this.markAsSelected(option);
    this.optionChanged.emit({
      label,
      value,
    });
  }

  findByValueAndMarkAsSelected(value: any) {
      const [matched] = this.dropdownOptions.filter((option) => option.value === value);
      if (matched) {
        this.selectedOption = {
          label: matched.label,
          value: matched.value,
        }
        this.markAsSelected(matched);
      }
  }

  private markAsSelected(option: SbDropdownOptionComponent) {
    this.dropdownOptions.forEach((opt) => opt.selected = false);
    setTimeout(() => option.selected = true);
  }

}
