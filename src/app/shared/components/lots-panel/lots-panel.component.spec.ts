import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotsPanelComponent } from './lots-panel.component';
import { LotCardComponent } from '@paperless/shared/components';
import { ImageresizePipe } from '@paperless/shared/pipes';

describe('Lots.PanelComponent', () => {
  let component: LotsPanelComponent;
  let fixture: ComponentFixture<LotsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotsPanelComponent, LotCardComponent, ImageresizePipe ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
