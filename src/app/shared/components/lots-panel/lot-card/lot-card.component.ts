import { Component, Input, OnInit, Output, EventEmitter, ElementRef } from '@angular/core';
import { BidClient, BidLot } from '@paperless/shared';

@Component({
  selector: 'app-lot-card',
  templateUrl: './lot-card.component.html',
  styleUrls: ['./lot-card.component.scss']
})
export class LotCardComponent implements OnInit {

  client: BidClient;
  isIndividual: Boolean;

  @Input() lot: BidLot;
  @Input() expanded = false;

  @Output()
  private selected: EventEmitter<BidLot> = new EventEmitter();

  constructor(public elementRef: ElementRef) {

  }

  cardSelected() {
    this.selected.emit(this.lot);
    return false;
  }

  ngOnInit() {
  }

}
