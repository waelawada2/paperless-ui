import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotCardComponent } from './lot-card.component';
import { ImageresizePipe } from '@paperless/shared/pipes';

describe('LotCardComponent', () => {
  let component: LotCardComponent;
  let fixture: ComponentFixture<LotCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotCardComponent, ImageresizePipe ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotCardComponent);
    component = fixture.componentInstance;
    component.lot = {} as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
