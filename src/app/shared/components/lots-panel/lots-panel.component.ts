import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BidLot } from '@paperless/shared/models';

@Component({
  selector: 'app-lots-panel',
  templateUrl: './lots-panel.component.html',
  styleUrls: ['./lots-panel.component.scss']
})
export class LotsPanelComponent implements OnInit {

  @Input() lots: BidLot[];

  @Output() private selectedLot: EventEmitter<BidLot> = new EventEmitter();

  private selected: BidLot;

  constructor() { }

  ngOnInit() {
  }

  handleSelected(lot: BidLot) {
    this.selected = lot;
    this.selectedLot.emit(lot);
  }

}
