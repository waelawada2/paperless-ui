import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserImageComponent } from './user-image.component';
import { GetUserImageUrlPipe } from '@paperless/shared/pipes';
import { ConfigModule } from '@paperless/shared/config';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('UserImageComponent', () => {
  let component: UserImageComponent;
  let fixture: ComponentFixture<UserImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserImageComponent, GetUserImageUrlPipe ],
      imports: [ ConfigModule, HttpClientTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
