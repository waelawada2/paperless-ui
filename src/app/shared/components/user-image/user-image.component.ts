import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-user-image',
  templateUrl: './user-image.component.html',
  styleUrls: ['./user-image.component.scss']
})
export class UserImageComponent implements OnInit {

  @Input() width: string;
  @Input() height: string;
  @Input() user: string;
  @ViewChild('userImage') userImageNative: ElementRef;
  constructor() { }

  ngOnInit() {
  }

  onImageLoadFail(event: Event) {
    const image = this.userImageNative.nativeElement as HTMLImageElement;
    image.style.display = 'none';
  }

}
