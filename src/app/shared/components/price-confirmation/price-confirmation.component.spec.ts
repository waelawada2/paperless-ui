import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceConfirmationComponent } from './price-confirmation.component';
import { KeypadComponent } from '@paperless/shared/components';
import { MoneyPipe } from '@paperless/shared/pipes';
import { CommonModule } from '@angular/common';

describe('PriceConfirmationComponent', () => {
  let component: PriceConfirmationComponent;
  let fixture: ComponentFixture<PriceConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceConfirmationComponent, KeypadComponent, MoneyPipe ],
      imports: [
        CommonModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
