import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-price-confirmation',
  templateUrl: './price-confirmation.component.html',
  styleUrls: ['./price-confirmation.component.scss']
})
export class PriceConfirmationComponent implements OnInit {

  @Input() currencyCode: string;
  @Input() keypadFor = 'bidder';
  @Input() confirmationLabel: String;

  @Output() cancel = new EventEmitter();
  @Output() priceConfirmed = new EventEmitter<string>();

  amount = '';
  fullAmountDeletion = false;
  submitted = false;

  get isAmountInvalid() {
    return this.amount === null || +this.amount <= 0;
  }

  constructor() { }

  ngOnInit() {
  }

  amountChanged($event) {
    // Do not allow 0 as first value
    if (!this.amount && $event.futureValue.indexOf('0') === 0) {
      $event.preventDefault();
      return;
    }
    if ($event.futureValue.length > 10) {
      $event.preventDefault();
      return;
    }
    this.amount = $event.futureValue;
    this.submitted = false;
  }

  submitAttempt() {
    this.submitted = true;
    if (!this.isAmountInvalid) {
      this.priceConfirmed.emit(this.amount);
    }
  }


}
