import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
export const graceFail = (width: {[key: string]: any} = {}) => <T>(source: Observable<T>) => {
    return source.pipe(
        map((response) => {
            return {
                hadErrors: false,
                ...response as any,
            };
        }),
        catchError(error => of({
            hadErrors: true,
            error,
            ...width,
        })),
    );
}
