import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleWithProviders } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import {
  AuctionService,
  BidClientService,
  LotService,
  FeatureStatusService,
  ReportService,
  LoadingScreenService,
} from './services';
import { ClientPartyType } from './client-party.type';
import {
  LotCardComponent,
  LotsPanelComponent,
  NavbarComponent,
  PriceConfirmationComponent,
  KeypadComponent,
  ErrorPanelComponent,
  SbDropdownComponent,
  SbDropdownOptionComponent,
  AuctionCardComponent,
  KeypadInputComponent,
  UserImageComponent,
  NavigationErrorComponent,
  AppDeciderComponent,
  PageNotFoundComponent,
 } from './components';
import {
  MoneyPipe,
  ImageresizePipe,
  GetUserImageUrlPipe,
  TrunkAtPipe
} from './pipes';
import { LoadingIndicatorComponent } from './components/loading-indicator/loading-indicator.component';
import { NavigationResolveErrorDirective } from './components/navigation-resolve-error.directive';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,
  ],
  exports: [
    LotsPanelComponent,
    LotCardComponent,
    NavbarComponent,
    ImageresizePipe,
    GetUserImageUrlPipe,
    TrunkAtPipe,
    KeypadComponent,
    ErrorPanelComponent,
    PriceConfirmationComponent,
    MoneyPipe,
    SbDropdownComponent,
    SbDropdownOptionComponent,
    AuctionCardComponent,
    KeypadInputComponent,
    LoadingIndicatorComponent,
    UserImageComponent,
    NavigationResolveErrorDirective,
    NavigationErrorComponent,
    AppDeciderComponent,
  ],
  declarations: [
    LotsPanelComponent,
    LotCardComponent,
    NavbarComponent,
    ImageresizePipe,
    GetUserImageUrlPipe,
    TrunkAtPipe,
    MoneyPipe,
    ErrorPanelComponent,
    KeypadComponent,
    PriceConfirmationComponent,
    SbDropdownComponent,
    SbDropdownOptionComponent,
    AuctionCardComponent,
    KeypadInputComponent,
    LoadingIndicatorComponent,
    UserImageComponent,
    NavigationResolveErrorDirective,
    NavigationErrorComponent,
    AppDeciderComponent,
    PageNotFoundComponent,
  ],
  entryComponents: [
    KeypadComponent,
    NavigationErrorComponent,
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        AuctionService,
        BidClientService,
        ClientPartyType,
        LotService,
        FeatureStatusService,
        ReportService,
        LoadingScreenService,
      ],
    }
  }
}
