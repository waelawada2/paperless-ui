
import { BidClient } from './models/bid-client.interface';
import { Injectable } from '@angular/core';

@Injectable()
export class ClientPartyType {

  assessPartyType(client: BidClient): Boolean {
    let isIndividual: Boolean;
    if (client.partyType) {
      switch (client.partyType) {
        case 'COMPANY':
        case 'ESTATE': isIndividual = false; break;
        case 'JOINT':
        case 'INDIVIDUAL': isIndividual = true; break;
        default: break;
      }
      return isIndividual;
    }
  }
}
