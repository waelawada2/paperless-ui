import { Injectable } from '@angular/core';
import { Configuration } from './configuration';

@Injectable()
export class ConfigService {

  private internalConfig: Configuration = {} as any;
  constructor() { }

  getAll(): Configuration {
    return this.getConfigFromCurrentEnv();
  }

  getValueOf(configKey: keyof Configuration) {
    const config = this.getConfigFromCurrentEnv()
    return config[configKey];
  }

  setConfiguration(configuration: Configuration) {
    this.internalConfig = configuration;
  }

  private getConfigFromCurrentEnv() {
      return this.internalConfig;
  }
}
