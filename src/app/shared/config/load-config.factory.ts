import { Configuration } from './configuration';
import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';
import { tap, take } from 'rxjs/operators';
export const loadConfigFactory = (http: HttpClient, configService: ConfigService) => {
    return () => {
        return new Promise((resolve, reject) => {
            http.get<Configuration>('./assets/config.json')
                .pipe(
                    take(1),
                    tap((config) => configService.setConfiguration(config))
                )
                .subscribe({
                    error: reject,
                    next: resolve,
                });
        });
    };
}

