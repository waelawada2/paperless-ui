import { NgModule, Optional, SkipSelf, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { loadConfigFactory } from './load-config.factory';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: loadConfigFactory,
      multi: true,
      deps: [HttpClient, ConfigService],
    },
  ]
})
export class ConfigModule {
  constructor(@Optional() @SkipSelf() configModule: ConfigModule) {
    if (configModule) {
      throw new Error('ConfigModule already imported and exported by SharedModule. Do not import it again');
    }
  }
}
