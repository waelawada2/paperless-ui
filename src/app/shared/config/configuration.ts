export interface Configuration {
    local: boolean;
    production: boolean,
    paperless_service_url: string,
    sothebysImageCdn: string,
    bidxKeycloakMatchingRole: string;
    hammerKeycloakMatchingRole: string;
    bidDepartmentKeycloakMatchingRole: string;
    'auctioneer-lot-refresh-interval': number,
    'keycloak.realm': string,
    'keycloak.url': string,
    'keycloak.resource': string,
    'keycloak.clientId': string,
    'keycloak.auth-server-url': string,
    'keycloak.ssl-required': string,
    'keycloak.public-client': boolean,
}
