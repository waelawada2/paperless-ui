import { Injectable } from '@angular/core';
import { ConfigService } from '@paperless/shared/config';
import { KeycloakInstance, KeycloakInitOptions } from 'keycloak-js';
import * as keycloak from 'keycloak-js';

@Injectable()
export class KeycloakService {
    private keyCloakInstance: KeycloakInstance;
    constructor(private configService: ConfigService) {

    }

    get(): KeycloakInstance | undefined {
        return this.keyCloakInstance;
    }

    init(instanceOpts?: Partial<KeycloakInstance>) {
        if (!this.keyCloakInstance) {
            const config = this.configService.getAll();
            this.keyCloakInstance = keycloak({
                realm: config['keycloak.realm'],
                url: config['keycloak.url'],
                resource: config['keycloak.resource'],
                clientId: config['keycloak.clientId'],
                'ssl-required': config['keycloak.ssl-required'],
                'public-client': config['keycloak.public-client'],
            });
            if (instanceOpts) {
                for (const key in instanceOpts) {
                    if (instanceOpts.hasOwnProperty(key)) {
                        this.keyCloakInstance[key] = instanceOpts[key];
                    }
                }
            }
        }
        return new Promise((resolve, reject) => {
            this.keyCloakInstance
                .init({} as KeycloakInitOptions)
                .success(() => resolve(true))
                .error(reject);
        });

    }
}
