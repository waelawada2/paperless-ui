import { Injectable } from '@angular/core';
import { KeycloakService } from './keycloak.service';
import { KeycloakInstance, KeycloakProfile } from 'keycloak-js';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { Observable } from 'rxjs/Observable';
import { map, switchMap, take } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';
import { ConfigService } from '@paperless/shared/config';


interface KeycloakProfileExt {
    userId?: string;
    attributes?: {
        [attr: string]: any[];
    },
    [key: string]: any;
}

@Injectable()
export class SessionService {

    private _cachedUser;
    private keyCloak: KeycloakInstance;

    constructor(private keyCloakTransporter: KeycloakService, private configService: ConfigService) {
        this.keyCloak = keyCloakTransporter.get();
    }

    get userProfile$(): Observable<KeycloakProfile & KeycloakProfileExt> {
        const userProfilePromise = new Promise<KeycloakProfile>((resolve, reject) => {
            if (this._cachedUser) {
                resolve(this._cachedUser);
                return;
            }
            this.keyCloak
                .loadUserProfile()
                .success((user) => {
                    this._cachedUser = user;
                    resolve(user);
                })
                .error(reject);
        });
        const profile$ = fromPromise(userProfilePromise).pipe(map((profile) => ({...profile, userId: this.keyCloak.subject})));
        return profile$.pipe(take(1));
    }

    hasHammerRole() {
        const hammerRole = this.configService.getValueOf('hammerKeycloakMatchingRole') as string;
        return this.hasUserRole(hammerRole);
    }

    hasBidxRole() {
        const hammerRole = this.configService.getValueOf('bidxKeycloakMatchingRole') as string;
        return this.hasUserRole(hammerRole);
    }

    hasReportsRole() {
        const hammerRole = this.configService.getValueOf('bidDepartmentKeycloakMatchingRole') as string;
        return this.hasUserRole(hammerRole);
    }

    getLoggedUserRoles() {
        const keycloak = this.keyCloakTransporter.get();
        if (!keycloak.realmAccess) {
            throw new Error(`User not logged in`);
        }
        return this.keyCloak.realmAccess.roles;
    }

    logout() {
        this.keyCloak.logout();
    }

    getUserAttrOrThrow(attr: string) {
        return this.userProfile$.pipe(switchMap((user) => {
            const requestedAttr = user.attributes[attr];
            if (!requestedAttr) {
                return _throw(`${attr} not found in user attributes`);
            }
            return of(requestedAttr);
        }));
    }

    private hasUserRole(role: string) {
        const roles = this.getLoggedUserRoles();
        return roles.indexOf(role) !== -1
    }
}
