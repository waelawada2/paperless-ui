import { NgModule } from '@angular/core';
import { SessionService } from '../session.service';
import { SessionService as MockedSessionService } from './session-service.mock';
import { KeycloakInstanceMock } from './keycloak-instance.mock';
import { KeycloakService } from '../keycloak.service';
import { KeycloakInstance } from 'keycloak-js';

@NgModule({
    providers: [
        {
            provide: SessionService,
            useClass: MockedSessionService,
        },
        {
            provide: KeycloakService,
            useClass: class extends KeycloakService {
                get() {
                    return new KeycloakInstanceMock as KeycloakInstance;
                }
                init() {
                    return Promise.resolve(null)
                }
            },
        },
    ]
})
export class SecurityTestingModule {
}
