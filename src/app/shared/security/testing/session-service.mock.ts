import { SessionService as BaseService } from '../session.service';
import { of } from 'rxjs/observable/of';
export class SessionService extends  BaseService {
    logout() {}
    get userProfile$() {
        return of({
            email: 'mocked@endava.com',
        });
    }
    getUserAttrOrThrow(attr: string) {
        return of(attr);
    }
}
