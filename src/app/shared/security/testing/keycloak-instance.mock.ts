import {
    KeycloakInitOptions,
    KeycloakLoginOptions,
    KeycloakPromise,
    KeycloakError,
    KeycloakProfile,
} from 'keycloak-js';
const createPromise = () => {
    const p: any = {
        setSuccess: function(result) {
            p.success = true;
            p.resolve(result);
        },

        setError: function(result) {
            p.success = false;
            p.reject(result);
        }
    };
    p.promise = new Promise(function(resolve, reject) {
        p.resolve = resolve;
        p.reject = reject;
    });
    p.promise.success = function(callback) {
        p.promise.then(callback);
        return p.promise;
    }
    p.promise.error = function(callback) {
        p.promise.catch(callback);
        return p.promise;
    }
    return p;
}

export class KeycloakInstanceMock {

    realmAccess = {
        roles: [],
    }

    init(initOptions: KeycloakInitOptions): KeycloakPromise<boolean, KeycloakError> {
        const promise = createPromise();
        promise.setSuccess()
        return promise.promise;
    }
    login(options?: KeycloakLoginOptions): KeycloakPromise<void, void> {
        const promise = createPromise();
        promise.setSuccess()
        return promise.promise;
    }
    logout(options?: any): KeycloakPromise<void, void> {
        const promise = createPromise();
        promise.setSuccess()
        return promise.promise;
    }

    loadUserProfile(): KeycloakPromise<KeycloakProfile, void> {
        const promise = createPromise();
        promise.setSuccess()
        return promise.promise;
    }
}
