
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { SessionService } from '../session.service';
import { roleCheckFail } from './role-check-fail';

@Injectable()
export class BidxRoleGuard implements CanActivate {

    constructor(private sessionService: SessionService, private router: Router) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!this.sessionService.hasBidxRole()) {
            return roleCheckFail(this.router);
        }
        return true;
    }

}
