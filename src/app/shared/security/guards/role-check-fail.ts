import { Router } from '@angular/router';

export const roleCheckFail = (router: Router) => {
    router.navigateByUrl('/'); // This will put AppDeciderComponent in place so that it decides where the user should go
    return false;
}
