
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { SessionService } from '../session.service';
import { roleCheckFail } from './role-check-fail';

@Injectable()
export class ReportsRoleGuard implements CanActivate {

    constructor(private sessionService: SessionService, private router: Router) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!this.sessionService.hasReportsRole()) {
            return roleCheckFail(this.router);
        }
        return true;
    }

}
