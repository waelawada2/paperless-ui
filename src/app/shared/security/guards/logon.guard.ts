import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { KeycloakService } from '../keycloak.service';

@Injectable()
export class LogonGuard implements CanActivate {

    constructor(private keyCloak: KeycloakService) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const keyCloak = this.keyCloak.get();
        if (!keyCloak.authenticated) {
            keyCloak.login();
        }
        return keyCloak.authenticated;
    }

}
