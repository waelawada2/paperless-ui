import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { switchMap } from 'rxjs/operators';
import { KeycloakService } from './keycloak.service';

@Injectable()
export class KeyCloakTokenInterceptor implements HttpInterceptor {
    constructor(private keyCloakService: KeycloakService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler) {
        const keyCloak = this.keyCloakService.get();
        if (keyCloak && keyCloak.token) {
            const promiseToken = new Promise<string>((resolve, reject) => {
                keyCloak
                    .updateToken(5)
                    .success(() => resolve(keyCloak.token))
                    .error(reject);
            });
            const token$ = fromPromise(promiseToken);
            return token$.pipe(switchMap((token) => {
                const req = request.clone({
                    setHeaders: {
                        Authorization: `Bearer ${token}`,
                    }
                })
                return next.handle(req);
            }));
        }
        return next.handle(request);
    }
}
