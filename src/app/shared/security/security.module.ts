import { NgModule, SkipSelf, Optional } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LogonGuard, BidxRoleGuard, HammerRoleGuard, ReportsRoleGuard, } from './guards';
import { SessionService } from './session.service';
import { KeyCloakTokenInterceptor } from './keycloak-token.interceptor';
import { KeycloakService } from './keycloak.service';

@NgModule({
    providers: [
        LogonGuard,
        BidxRoleGuard,
        HammerRoleGuard,
        ReportsRoleGuard,
        SessionService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: KeyCloakTokenInterceptor,
            multi: true,
        },
        KeycloakService,
    ],
})
export class SecurityModule {

    constructor(@SkipSelf() @Optional() securityModule: SecurityModule) {
        if (securityModule) {
            throw new Error('SecurityModule already loaded. Do not import it again');
        }
    }

}
