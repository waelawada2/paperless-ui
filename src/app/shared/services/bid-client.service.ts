import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { BidClient } from '@paperless/shared/models';
import { ConfigService } from '@paperless/shared/config';

@Injectable()
export class BidClientService {

  private readonly serviceContext = 'client';
  private serviceUrl = this.config.getValueOf('paperless_service_url');

  constructor(private http: HttpClient, private config: ConfigService) { }

  get baseUrl() {
    return `${this.serviceUrl}/${this.serviceContext}`
  }

  getClientDataForLot(clientId: string, auctionId: string, sessionNumber: number, lotId: string) {
    const queryParams = `auctionId=${auctionId}&sessionNumber=${sessionNumber}&lotId=${lotId}`;
    const clientUrl = `${this.baseUrl}/${clientId}?${queryParams}`;
    return this.http.get<{ client: BidClient }>(clientUrl);
  }

}
