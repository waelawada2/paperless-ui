import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { tap, finalize } from 'rxjs/operators';

@Injectable()
export class LoadingScreenService {
    private loadingSubject = new Subject<boolean>();
    loadingQueue$ = this.loadingSubject.asObservable();
    display() {
        this.loadingSubject.next(true);
    }
    hide() {
        this.loadingSubject.next(false);
    }

    /**
     *  Utility method to display a loading indicator in observables (common use is http calls)
     * @param mills how long will it wait until it display the loading indicator
     */
    asRxJsOperator(mills: number = 1000) {
        let timeoutId;
        let wasDisplayed = false;
        return <T>(source: Observable<T>): Observable<T> => {
            timeoutId = setTimeout(() => {
                this.display();
                wasDisplayed = true;
            }, mills);
            return source
                .pipe(
                    finalize(() => {
                        clearTimeout(timeoutId);
                        if (wasDisplayed) {
                            this.hide();
                        }
                    }),
            )
        };
    }
}
