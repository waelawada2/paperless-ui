import { TestBed, inject } from '@angular/core/testing';

import { AuctionService } from './auction.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigModule } from '@paperless/shared/config';

describe('AuctionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuctionService],
      imports: [
        HttpClientTestingModule,
        ConfigModule,
      ]
    });
  });

  it('should be created', inject([AuctionService], (service: AuctionService) => {
    expect(service).toBeTruthy();
  }));
});
