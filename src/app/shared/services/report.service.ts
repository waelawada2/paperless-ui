import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '@paperless/shared/config';

@Injectable()
export class ReportService {

  private readonly serviceContext = 'reports';
  private serviceUrl = this.config.getValueOf('paperless_service_url');

  constructor(private http: HttpClient, private config: ConfigService) { }

  get baseUrl() {
    return `${this.serviceUrl}/${this.serviceContext}`
  }

  getCSVReport(auctionId: string, sessionNumber: number = 1) {
    return (`${this.baseUrl}/download?auctionId=${auctionId}&sessionNumber=${sessionNumber}`);
  }

}
