import { TestBed, inject } from '@angular/core/testing';

import { BidClientService } from './bid-client.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigModule } from '@paperless/shared/config';

describe('BidClientService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BidClientService],
      imports: [
        HttpClientTestingModule,
        ConfigModule,
      ]
    });
  });

  it('should be created', inject([BidClientService], (service: BidClientService) => {
    expect(service).toBeTruthy();
  }));
});
