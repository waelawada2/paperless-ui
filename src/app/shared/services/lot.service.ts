import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BidClient, BidLot } from '@paperless/shared';
import { ConfigService } from '@paperless/shared/config';


@Injectable()
export class LotService {

    readonly baseUrl = this.config.getValueOf('paperless_service_url');
    get serviceUrl() {
        return `${this.baseUrl}/auctions`
    }
    constructor(private http: HttpClient, private config: ConfigService) {

    }

    getLotInfoFromAuction(auctionId: string, session: number, lotId: string) {
        return this.http.get<BidLot>(`${this.serviceUrl}/${auctionId}/lots/${lotId}?sessionNumber=${session}`);
    }

    setLotAsSold(
        auctionId: string,
        auctionSession: number,
        lotId: string,
        result: {
            hammerPrice: number,
            paddleNumber: string,
        },
    ) {
        return this.setAuctioneerLotResult(
            auctionId,
            auctionSession,
            lotId,
            'SLD',
            result,
        );
    }

    setLotAsUnsold(
        auctionId: string,
        auctionSession: number,
        lotId: string,
        result: {
            hammerPrice: number,
            paddleNumber: string,
        },
    ) {
        return this.setAuctioneerLotResult(
            auctionId,
            auctionSession,
            lotId,
            'BI',
            result,
        );
    }

    setLotAsWon(
        client: Partial<BidClient>,
        amount: number,
        auctionId: string,
        lotId: string,
        sessionNumber = 1) {
       return this.setLotResult(client, amount, auctionId, lotId, sessionNumber, 'WON');
    }

    setLotAsNoBid(
        client: Partial<BidClient>,
        auctionId: string,
        lotId: string,
        sessionNumber = 1) {
       return this.setLotResult(client, 0, auctionId, lotId, sessionNumber, 'NOBID');
    }

    setLotAsUnderbid(
        client: Partial<BidClient>,
        amount: number,
        auctionId: string,
        lotId: string,
        sessionNumber = 1) {
       return this.setLotResult(client, amount, auctionId, lotId, sessionNumber, 'UNDERBID');
    }

    private setLotResult(
        client: Partial<BidClient>,
        amount: number,
        auctionId: string,
        lotId: string,
        sessionNumber: number,
        result: 'WON' | 'UNDERBID' | 'NOBID') {

        const url = `${this.serviceUrl}/${auctionId}/lots/${lotId}/executions?&sessionNumber=${sessionNumber}`;
        return this.http.post<Partial<BidLot>>(url, {
            client,
            result,
            amount,
        });
    }

    private setAuctioneerLotResult(
        auctionId: string,
        auctionSession: number,
        lotId: string,
        status: 'SLD' | 'BI',
        result: {
            hammerPrice: number,
            paddleNumber: string,
        },
    ) {
       return this.http.post(
           `${this.serviceUrl}/${auctionId}/lots/${lotId}?sessionNumber=${auctionSession}`,
           {
             ...result,
             status,
           }
       );

    }

}
