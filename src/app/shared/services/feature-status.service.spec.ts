import { TestBed, inject } from '@angular/core/testing';

import { FeatureStatusService } from './feature-status.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigModule } from '@paperless/shared/config';

describe('FeatureStatusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FeatureStatusService],
      imports: [
        HttpClientTestingModule,
        ConfigModule,
      ]
    });
  });

  it('should be created', inject([FeatureStatusService], (service: FeatureStatusService) => {
    expect(service).toBeTruthy();
  }));
});
