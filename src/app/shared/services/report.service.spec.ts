import { TestBed, inject } from '@angular/core/testing';

import { ReportService } from './report.service';
import { ConfigModule } from '@paperless/shared/config';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ReportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReportService],
      imports: [
        ConfigModule,
        HttpClientTestingModule
      ]
    });
  });

  it('should be created', inject([ReportService], (service: ReportService) => {
    expect(service).toBeTruthy();
  }));
});
