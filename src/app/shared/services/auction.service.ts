import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Auction, BidLot, Location as AuctionLocation, AuctionState } from '@paperless/shared';
import { ConfigService } from '@paperless/shared/config';

@Injectable()
export class AuctionService {

  private readonly serviceContext = 'auctions';
  private serviceUrl = this.config.getValueOf('paperless_service_url');

  constructor(private http: HttpClient, private config: ConfigService) { }

  get baseUrl() {
    return `${this.serviceUrl}/${this.serviceContext}`
  }

  getAuctionCardsByAgent() {
    return this.http.get<{ cards: Auction[], locations: AuctionLocation[] }>(`${this.baseUrl}/cardsByAgent`);
  }

  getAuctionsCardsAndLocations(state?: AuctionState) {
    const auctionsUrl = state ? `${this.baseUrl}/cards?state=${state}` : `${this.baseUrl}/cards`;
    return this.http.get<{ cards: Auction[], locations: AuctionLocation[] }>(auctionsUrl);
  }

  getLotsAndAuctionByAgent(auctionId: string, sessionNumber = 1) {
    const queryParams = `sessionNumber=${sessionNumber}&showAssignedOnly=true`;
    const lotsUrl = `${this.baseUrl}/${auctionId}/lots?${queryParams}`;
    return this.http.get<{ lots: BidLot[], auction: Auction }>(lotsUrl);
  }

  getLotsPerClient(clientId: number, auctionId: string, sessionNumber = 1) {
    const queryParams = `sessionNumber=${sessionNumber}&clientId=${clientId}`;
    const lotsUrl = `${this.baseUrl}/${auctionId}/lots?${queryParams}`;
    return this.http.get<{ lots: BidLot[] }>(lotsUrl);
  }

  getAllLotsAndAuction(auctionId: string, sessionNumber = 1) {
    const queryParams = `sessionNumber=${sessionNumber}`;
    const lotsUrl = `${this.baseUrl}/${auctionId}/lots?${queryParams}`;
    return this.http.get<{ lots: BidLot[], auction: Auction }>(lotsUrl);
  }

  openAuctionSession(auction: Auction, openAuctionTried: number) {
    return this.changeAuctionSessionState({ ...auction, openAuctionTried }, 'OPEN');
  }

  closeAuctionSession(auction: Auction) {
    return this.changeAuctionSessionState(auction, 'CLOSED');
  }

  private changeAuctionSessionState(auction: Auction & {[key: string]: any}, state: 'OPEN' | 'CLOSED') {
    return this.http.post(`${this.baseUrl}/state`, {
      ...auction,
      state
    });
  }

}
