import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from '@paperless/shared/config';

@Injectable()
export class FeatureStatusService {

  private readonly serviceContext = 'featureStatus';

  private serviceUrl = this.config.getValueOf('paperless_service_url');

  constructor(private http: HttpClient, private config: ConfigService) { }

  get baseUrl() {
    return `${this.serviceUrl}/${this.serviceContext}`
  }

  isFeatureEnabled(feature: string) {
    return this.http.get<boolean>(`${this.baseUrl}/${feature}`);
  }

}
