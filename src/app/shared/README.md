# paperless-angular-shared

This a shared lib between the paperless projects in angular (bidder app and auctioneer's book - aka the "sister apps").

To commit changes to this shared lib, from any of those projects do:

1) Add this repo as a remote called shared:
`git remote add shared git@bitbucket.org:sothebys/paperless-angular-shared.git`

2) Push changes to develop branch:

`$ git subtree push --prefix=src/app/shared shared develop`

3) Update sister app project with version on develop:

`$ git subtree pull --prefix=src/app/shared shared develop --squash`
