import { GetUserImageUrlPipe } from './get-image-url.pipe';
import { ConfigService  } from '@paperless/shared/config';

describe('GetImageUrlPipe', () => {
  it('create an instance', () => {
    const pipe = new GetUserImageUrlPipe(new ConfigService());
    expect(pipe).toBeTruthy();
  });
});
