import { TrunkAtPipe } from './trunk-at.pipe';

describe('TrunkAtPipe', () => {
  let pipe: TrunkAtPipe;
  beforeEach(() => pipe = new TrunkAtPipe());
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('should not trunk before giving params', () => {
    expect(pipe.transform('random text', 60)).toBe('random text');
  });
  it('should trunk at given param without ellipsis', () => {
    expect(pipe.transform('sothebys2', 'sothebys'.length, false)).toBe('sothebys')
  });
  it('should trunk at given param with ellipsis', () => {
    expect(pipe.transform('sothebys2', 'sothebys'.length, true)).toBe('sothebys...')
  })
});
