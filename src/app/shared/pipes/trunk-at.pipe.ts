import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trunkAt'
})
export class TrunkAtPipe implements PipeTransform {

  transform(value: string, at: number, displayEllipsis: boolean = true): any {
    if (!value) {
      return null;
    }
    if (value.length <= at) {
      return value;
    }
    const trunkedValue = value.substring(0, at);
    return `${trunkedValue}${(displayEllipsis ? '...' : '')}`;
  }

}
