import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imageresize'
})
export class ImageresizePipe implements PipeTransform {

  transform(value: string, width: number, height: number): any {
    return `${value}.rend.${width}.${height}.jpg`;
  }

}
