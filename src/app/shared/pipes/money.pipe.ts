import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';
@Pipe({
    name: 'money'
})
export class MoneyPipe implements PipeTransform {
    private decimalPipe: DecimalPipe;
    constructor(@Inject(LOCALE_ID)locale: string) {
        this.decimalPipe = new DecimalPipe(locale);
    }
    transform(value: any) {
        return this.decimalPipe.transform(value, '1.0-0');
    }
}
