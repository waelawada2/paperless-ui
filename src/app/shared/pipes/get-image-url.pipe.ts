import { Pipe, PipeTransform } from '@angular/core';
import { ConfigService } from '@paperless/shared/config';

@Pipe({
  name: 'getUserImageUrl'
})
export class GetUserImageUrlPipe implements PipeTransform {

  constructor(private configService: ConfigService) {

  }

  transform(userName?: string, dimension?: string, filters?: string, ext = 'jpg'): any {
    if (!userName) {
      return null;
    }
    const url = this.configService.getValueOf('sothebysImageCdn');
    let composedUrl = `${url}/unsafe`;
    if (dimension) {
      composedUrl = `${composedUrl}/${dimension}`;
    }
    if (filters) {
      composedUrl = `${composedUrl}/filters:${filters}`;
    }
    composedUrl = `${composedUrl}/paperless/users/${userName}.${ext}`;
    return composedUrl;
  }

}
