import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ConfigModule } from '@paperless/shared/config';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SecurityTestingModule } from '@paperless/shared/security/testing';
import { SharedModule } from '@paperless/shared';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        ConfigModule,
        SecurityTestingModule,
        SharedModule.forRoot(),
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
