import { Injectable, } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuctionService } from '@paperless/shared/services';

@Injectable()
export class ReportsAuctionCardsResolver implements Resolve<any> {
    constructor(private auctionService: AuctionService) {

    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.auctionService.getAuctionsCardsAndLocations('CLOSED');
    }
}
