import { Component, OnInit, ViewChildren, QueryList, Inject } from '@angular/core';
import { AuctionCardComponent } from '@paperless/shared/components';
import { Auction } from '@paperless/shared';
import { ActivatedRoute } from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  constructor (@Inject(DOCUMENT) private document: Document) {}

  ngOnInit() {
    this.document.title = 'HAMMER';
  }
}
