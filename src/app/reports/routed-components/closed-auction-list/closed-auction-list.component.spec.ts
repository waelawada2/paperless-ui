import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClosedAuctionListComponent } from './closed-auction-list.component';
import { SharedModule } from '@paperless/shared';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs/observable/of';
import { ReportService } from '@paperless/shared/services';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigModule } from '@paperless/shared/config';
import { ToastrModule } from 'ngx-toastr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('ClosedAuctionListComponent', () => {
  let component: ClosedAuctionListComponent;
  let fixture: ComponentFixture<ClosedAuctionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClosedAuctionListComponent ],
      imports: [
        SharedModule.forRoot(),
        HttpClientTestingModule,
        ConfigModule,
        ToastrModule.forRoot(),
        NgbModule.forRoot(),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              auctionsCardsAndLocations: {
                cards: [],
                locations: [],
              }
            })
          }
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosedAuctionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
