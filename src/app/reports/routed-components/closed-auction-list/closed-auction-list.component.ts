import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { AuctionCardComponent } from '@paperless/shared/components';
import { Auction } from '@paperless/shared';
import { ActivatedRoute } from '@angular/router';
import { ReportService } from '@paperless/shared/services';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-closed-auction-list',
  templateUrl: './closed-auction-list.component.html',
  styleUrls: ['./closed-auction-list.component.scss']
})
export class ClosedAuctionListComponent implements OnInit {

  @ViewChildren(AuctionCardComponent) auctionCards: QueryList<AuctionCardComponent>;

  closedAuctionCards: Auction[] = [];
  private _auctionCards: Auction[];
  locations: string[] = [];
  selectedAuction: { auctionId: String, sessionId: number } = {} as any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private reportService: ReportService,
    private toastrService: ToastrService,
  ) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data) => {
      const { cards, locations } = data.auctionsCardsAndLocations;
      this._auctionCards = this.closedAuctionCards = cards;
      this.locations = locations;
    });
  }

  getUrl(auction: Auction) {
    return this.reportService.getCSVReport(auction.auctionId, auction.session);
  }

  clickHandler(auction: Auction) {
    this.toastrService.info('Final Sale Report is being downloaded.');
    this.selectedAuction.auctionId = auction.auctionId;
    this.selectedAuction.sessionId = auction.session;
  }

  onLocationFilterChange(location: string) {
    if (location === 'All') {
      this.closedAuctionCards = [...this._auctionCards];
    } else {
      this.closedAuctionCards = this._auctionCards.filter(auction => auction.location === location)
    }
  }

}

