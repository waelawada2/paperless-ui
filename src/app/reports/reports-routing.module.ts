import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogonGuard, ReportsRoleGuard } from '@paperless/shared/security';
import { ReportsComponent, ClosedAuctionListComponent } from '@paperless/reports/routed-components';
import { ReportsAuctionCardsResolver } from '@paperless/reports/resolvers';

const routes: Routes = [{
  path: '',
  component: ReportsComponent,
  canActivate: [LogonGuard, ReportsRoleGuard],
  children: [
    {
      path: '',
      pathMatch: 'full',
      component: ClosedAuctionListComponent,
      resolve: { auctionsCardsAndLocations: ReportsAuctionCardsResolver },
    }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    ReportsAuctionCardsResolver,
  ]
})
export class ReportsRoutingModule { }
