import { NgModule } from '@angular/core';
import { ReportsRoutingModule } from './reports-routing.module';
import { SharedModule } from '@paperless/shared';
import { CommonModule } from '@angular/common';
import {
    ReportsComponent
  } from './routed-components';
import { ClosedAuctionListComponent } from './routed-components/closed-auction-list/closed-auction-list.component';

@NgModule({
    declarations: [
        ReportsComponent,
        ClosedAuctionListComponent,
    ],
    imports: [
        SharedModule,
        ReportsRoutingModule,
        CommonModule,
    ],
})
export class ReportsModule { }
