/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}


interface Array<T> {
  unique: () => Array<T>;
}